package firstCodes;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class HelloWorldFx extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Label label = new Label("What's your name:");
        TextField text = new TextField();
        Label label1 = new Label("Hello ");
        Button button = new Button("HI !!!");

        button.setOnAction(e -> {
            //System.out.println("Button was clicked!");
            label1.setText("Hello " + text.getText());
        });

        VBox box = new VBox();
        box.setAlignment(Pos.CENTER);
        box.getChildren().addAll(label, text, button, label1);

        primaryStage.setTitle("Hello!");
        primaryStage.setScene(new Scene(box, 300, 200));
        primaryStage.show();
    }
}
