package firstCodes;

import java.sql.SQLOutput;
import java.util.Scanner;

public class FirstProgram {

    public static void main(String[] args) {
//        dzialania();
//        parzystosc();
//        podzielnosc();
//        doSzescianu();
//        piecPierwszychLiter();
//        znakiDlaKodow();
//        przedzialLiczbowy();
//        oceny();
//        arabskaNaRzymska();
        sprawdzanieUnicode();
//        dzienTygodnia();
    }

    static void dzialania() {
        int a = 10, b = 20;
        int c = a * b;
        Scanner wpisz = new Scanner(System.in);
        System.out.println("Wpisz a:");
        a = wpisz.nextInt();
        System.out.println("Wpisz b:");
        b = wpisz.nextInt();
        System.out.println(a + "+" + b + "=" + (a + b));
        System.out.println(a + "-" + b + "=" + (a - b));
        System.out.println(a + "*" + b + "=" + (+a * b));
    }

    static void parzystosc() {
        int a;
        boolean b;
        Scanner wpisz = new Scanner(System.in);
        System.out.println("Wpisz liczbe do sprawdzenia parzystosci:");
        a = wpisz.nextInt();
        b = (a % 2) == 0;
        if (b == true) {
            System.out.println(a + " jest parzysta");
        } else {
            System.out.println(a + " jest nieparzysta");
        }
        ;
    }

    static void podzielnosc() {
        int liczba;
        System.out.println("Wpisz liczbe do sprawdzenia podzielnosci przez 5 i 3");
        Scanner wpisz = new Scanner(System.in);
        liczba = wpisz.nextInt();
        if (((liczba % 5) == 0) && ((liczba % 3) == 0)) {
            System.out.println(liczba + " jest podzielna przez 5 i przez 3");
        } else {
            System.out.println(liczba + " NIE jest podzielna przez 5 i przez 3");
        }
    }

    static void doSzescianu() {
        double x, liczba;
        Scanner tekst = new Scanner(System.in);
        System.out.println("Wpisz liczbe ktora ma byc podniesiona do szescianu:");
        liczba = tekst.nextDouble();
        x = Math.pow(liczba, 3);
        System.out.printf("Liczba %,.0f do szescianu to: %,.0f", liczba, x);
    }

    static void piecPierwszychLiter() {
        char lac, heb, tyb;
        //Lac zaczyna się od: 65), heb zaczyna się od: 1488) i tyb zaczyna się od: 3840
        lac = 65;
        heb = 1488;
        tyb = 3840;
        System.out.println("Lacinski     Hebraiski     Tybetanski");
        for (int counter = 0; counter <= 4; counter++) {
            System.out.printf("    %C            %C              %C \n", lac, heb, tyb);
            lac++;
            heb++;
            tyb++;
        }
    }

    static void znakiDlaKodow() {
        char[] znaki = new char[]{74, 65, 86, 32, 8658, 9786};
        System.out.println();
        for (int x = 0; x < 6; x++) {
            System.out.print(znaki[x]);
        }
    }

    static void przedzialLiczbowy() {
        int x;
        Scanner text = new Scanner(System.in);
        System.out.println("Wpisz liczbe do sprawdzenia przedzialu: ");
        x = text.nextInt();

        if (x > 10000) {
            System.out.println("Liczba " + x + " jest wieksza od 10.000");
        } else if ((x <= 10000) && (x >= 1001)) {
            System.out.println("Liczba " + x + " jest w przedziale od 1001 do 10000");
        } else if ((x < 1001) && (x >= 101)) {
            System.out.println("Liczba " + x + " jest w przedziale od 101 do 1000");
        } else if ((x < 101) && (x >= 11)) {
            System.out.println("Liczba " + x + " jest w przedziale od 11 do 100");
        } else if ((x < 11) && (x >= 1)) {
            System.out.println("Liczba " + x + " jest w przedziale od 1 do 10");
        } else {
            System.out.println("Liczba " + x + " jest w przedziale od 1 do 10");
        }
    }

    static void oceny() {
        byte ocena;
        Scanner text = new Scanner(System.in);
        System.out.println("Podaj cyfre oznaczajaca ocene:");
        ocena = text.nextByte();
        switch (ocena) {
            case 1:
                System.out.println(ocena + " oznacza NIEDOSTATECZNY");
                break;
            case 2:
                System.out.println(ocena + " oznacza DOPUSZCZAJACY");
                break;
            case 3:
                System.out.println(ocena + " oznacza DOSTATECZNY");
                break;
            case 4:
                System.out.println(ocena + " oznacza DOBRY");
                break;
            case 5:
                System.out.println(ocena + " oznacza BARDZO DOBRY");
                break;
            case 6:
                System.out.println(ocena + " oznacza CELUJACY");
                break;
        }
    }

    static void arabskaNaRzymska() {
        int liczba;
        Scanner tekst = new Scanner(System.in);
        System.out.println("Podaj arabska cyfre 1-5 ");
        liczba = tekst.nextInt();
        switch (liczba) {
            case 1:
                System.out.println("Arabska " + liczba + " to Rzymska I");
                break;
            case 2:
                System.out.println("Arabska " + liczba + " to Rzymska II");
                break;
            case 3:
                System.out.println("Arabska " + liczba + " to Rzymska III");
                break;
            case 4:
                System.out.println("Arabska " + liczba + " to Rzymska IV");
                break;
            case 5:
                System.out.println("Arabska " + liczba + " to Rzymska V");
                break;
            default:
                System.out.println("Cyfra rzymska jest nieprawidlowa");

        }
    }

    static void sprawdzanieUnicode() {
        /**
         * a-z kod 97-122
         * 0-9 kod 48-57
         * A-Z kod 65-90
         */
        Scanner text = new Scanner(System.in);
        int a;
        char b;
        System.out.println("Wpisz kod Unicode:");
        a = text.nextInt();
        b = (char) a;
        if ((a >= 48) && (a <= 57)) {
            System.out.println("Podany kod jest liczba z przedzialu: 0-9");
            System.out.println("Kod Unicode: \"" + a + "\" reprezentuje znak: \"" + b + "\"");
        } else if ((a >= 97) && (a <= 122)) {
            System.out.println("Podany kod jest mala litera ");
            System.out.println("Kod Unicode: \"" + a + "\" reprezentuje znak: \"" + b + "\"");
        } else if ((a >= 65) && (a <= 90)) {
            System.out.println("Podany kod jest duza litera ");
            System.out.println("Kod Unicode: \"" + a + "\" reprezentuje znak: \"" + b + "\"");
        } else {
            System.out.println("Podany kod nie jest z zadnego zakresow: a-z, A-Z, 0-9");
            System.out.println("Kod Unicode: \"" + a + "\" reprezentuje znak: \"" + b + "\"");
        }
    }

    static void dzienTygodnia() {
        int numerDnia;
        Scanner text = new Scanner(System.in);

        System.out.println("Wpisz numer dnia tygodznia 1-7:");
        numerDnia = text.nextInt();

        switch (numerDnia) {
            case 1:
                System.out.println("Pierwszy dzien tygodnia to PONIEDZIALEK");
                System.out.println("Zostalo " + (6 - numerDnia) + " dni do weekendu");
                break;
            case 2:
                System.out.println("Drugi dzien tygodnia to WTOREK");
                System.out.println("Zostalo " + (6 - numerDnia) + " dni do weekendu");
                break;
            case 3:
                System.out.println("Trzeci dzien tygodnia to SRODA");
                System.out.println("Zostalo " + (6 - numerDnia) + " dni do weekendu");
                break;
            case 4:
                System.out.println("Czwarty dzien tygodnia to CZWARTEK");
                System.out.println("Zostalo " + (6 - numerDnia) + " dni do weekendu");
                break;
            case 5:
                System.out.println("Piaty dzien tygodnia to PIATEK");
                System.out.println("Zostalo " + (6 - numerDnia) + " dni do weekendu");
                break;
            case 6:
                System.out.print("Szosty dzien tygodnia to SOBOTA");
                System.out.println("  - WEEKEND!");
                break;
            case 7:
                System.out.print("Siodmy dzien tygodnia to NIEDZIELA");
                System.out.println("  - WEEKEND");
                break;
            default:
                System.out.println("Nieporawny numer dnia tygodnia!");
        }
    }
}

