package firstCodes;

import java.util.Scanner;

public class ComputerPrice {

    private static final double VAT = 0.23;

    public static void main(String[] args) {
        double motherboard, ram, hdd, monitor, procesor;
        double computerPrice, monitorPrice, total;

        Scanner text = new Scanner(System.in);
        System.out.println("Podaj cenę plyty glownej: ");
        motherboard = text.nextDouble();
        System.out.println("Podaj cenę RAM: ");
        ram = text.nextDouble();
        System.out.println("Podaj cenę HDD ");
        hdd = text.nextDouble();
        System.out.println("Podaj cenę procesora ");
        procesor = text.nextDouble();
        System.out.println("Podaj cenę monitora ");
        monitor = text.nextDouble();

        computerPrice = getComputerPrice(motherboard, ram, hdd, procesor);
        System.out.printf("cena komputera bez monitora: $%,.2f; z VAT $%,.02f\n", computerPrice,computerPrice+computerPrice*VAT);
        monitorPrice = getMonitorPrice(monitor);
        System.out.printf("cena monitora: $%,.2f; z VAT $%,.02f\n", monitorPrice,monitorPrice+monitorPrice*VAT);
        total = getComputerAndMonitorPrice(motherboard, ram, hdd, procesor,monitor);
        System.out.printf("cena komputera z monitorem: $%,.2f; z VAT $%,.02f\n", total,total+total*VAT);
    }

    static double getComputerPrice(double motherboard, double ram, double hdd, double procesor) {
        return (motherboard + ram + hdd + procesor);
    }

    static double getMonitorPrice(double monitor) {
        return monitor;
    }

    static double getComputerAndMonitorPrice(double motherboard, double ram, double hdd, double procesor,double monitor) {
        return (getMonitorPrice(monitor)+getComputerPrice(motherboard , ram, hdd, procesor));
    }

}
