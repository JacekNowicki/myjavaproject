package packexercise;

public class Family {
    private Person father;
    private Person son;
    protected Family(Person father, Person son) {
        this.father = father;
        this.son = son;
    }
    protected Person getFather() {
        return father;
    }
    protected String getFatherName() {
        return father.getName();
    }
    protected Person getSon() {
        return son;
    }
}

