package packexercise;

public class FamilyTest {
    public static void main(String[] args) {

        Person fatherNowak = new Person("Jan", "Nowak", 45);
        Person sonNowak = new Person("Amadeusz", "Nowak", 16);


        System.out.println("Moj ojciec to "+fatherNowak);
        Family familyNowak = new Family(fatherNowak, sonNowak);

        Person fatherKowalski = new Person("Mieczysław", "Kowalski", 45);
        Person sonKowalski = new Person("Witosław", "Kowalski", 16);
        Family familyKowalski = new Family(fatherKowalski, sonKowalski);
    }
}