package packexercise;

public class Person {
    private String name;
    private String surname;
    private int age;

    protected Person(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }
    protected String getName() {
        return name;
    }
    protected String getSurname() {
        return getName() + surname;
    }


    public String toString() {
        return name +" "+surname;
    }

    protected int getAge() {
        return age;

    }
}
