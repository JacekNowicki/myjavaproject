package threadRunExercises.fromLesson;

import java.math.BigInteger;

public class Factor implements Runnable {
    private int num;

    public Factor(int num) {
        this.num = num;
    }

    @Override
    public void run() {
        BigInteger factor = factorOf();
        System.out.println("Factor of: " + num + " = " + factor);
    }

    private BigInteger factorOf() {
        BigInteger factorial = BigInteger.valueOf(1);
        if (0 == num) {
            return factorial;
        }
        for (int i = 1; i <= num; i++) {
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }
        return factorial;
    }
}
