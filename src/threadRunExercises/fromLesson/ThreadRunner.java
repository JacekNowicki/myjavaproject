package threadRunExercises.fromLesson;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ThreadRunner {
    private static long FACTOR_OF = 6;

    public static void main(String[] args) throws InterruptedException {
        System.out.println(Thread.currentThread().getName());

        // #1
    //    customThread();

        // #2
      //  runnable();

        // #3
        executorService();

        // #4
     //   standardWay();
    }

    private static void customThread() {
        System.out.println("Start CustomThread!");
        CustomThread customThread = new CustomThread("Custom Thread");
        customThread.start();
        System.out.println("Done!");
    }

    private static void runnable() {
        System.out.println("Start CustomRunnable!");
        CustomRunnable customRunnable = new CustomRunnable();
        Thread thread1 = new Thread(customRunnable);
        thread1.start();
        Thread thread2 = new Thread(customRunnable);
        thread2.start();
        System.out.println("Done!");
    }

    private static void executorService() throws InterruptedException {
       // System.out.println("!!! Processors: " + Runtime.getRuntime().availableProcessors());

        long start = System.currentTimeMillis();
        System.out.println("Start ExecutorService!");
        ExecutorService executorService = Executors.newFixedThreadPool(8);
        for (int i = 1; i <= FACTOR_OF; i++) {
            executorService.execute(new Factor(i));
        }
        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        System.out.println("Done!");
        long end = System.currentTimeMillis();
        System.out.println("Time: " + (end - start) + " ms");
    }

    private static void standardWay() {
        long start = System.currentTimeMillis();
        System.out.println("Start standard way!");
        for (int i = 1; i <= FACTOR_OF; i++) {
            Factor factor = new Factor(i);
            factor.run();
        }
        long end = System.currentTimeMillis();
        System.out.println("Done!");
        System.out.println("Time: " + (end - start) + " ms");
    }
}
