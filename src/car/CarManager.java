package car;


public class CarManager {

    public static void main(String[] args) {

        Entertainment extras1 = new Entertainment(true,22,false);
        Entertainment extras2 = new Entertainment(false,1,true);

        extras1.setGpsPresent(true);

        Car car1 =new Car("Toyota","white",extras1);
        Car car2 =new Car("BMW","black",extras2);


        car1.setSpeed(50);
        car2.setSpeed(75);

        System.out.println(car1);
        System.out.println(car2);

        car1.start();
        car2.start();

        car1.setSpeed(50);
        car2.setSpeed(75);

        System.out.println(car1);
        System.out.println(car2);

    }

}

