package car;

public class Entertainment {

    private boolean cdOn;
    private boolean gpsPresent;
    private int volume;

    public Entertainment() {
    }

    public boolean isCdOn() {
        return cdOn;
    }

    public boolean isGpsPresent() {
        return gpsPresent;
    }

    public int getVolume() {
        return volume;
    }

    public Entertainment(boolean cdOn, int volume, boolean gpsPresent) {
        this.cdOn = cdOn;
        this.volume = volume;
        this.gpsPresent=gpsPresent;
    }

    public void setCdOn(boolean cdOn) {
        this.cdOn = cdOn;
    }

    public void setGpsPresent(boolean gpsPresent) {
        this.gpsPresent = gpsPresent;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    @Override
    public String toString() {
        return "Entertainment{" +
                "cdOn=" + cdOn +
                ", volume=" + volume +
                ", gpsPresent=" + gpsPresent +
                '}';
    }
}
