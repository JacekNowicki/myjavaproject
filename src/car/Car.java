package car;

public class Car extends Entertainment{
    private String brand;
    private String color;
    private int speed;
    private Entertainment extras;

    private boolean isMoving = false;

    public Car() {
    }

    public Car(String brand, String color, Entertainment extras) {
        this.brand = brand;
        this.color = color;
        this.extras = extras;
    }

    public Car(String color) {
        this.color = color;
    }

    public String getBrand() {
        return brand;
    }

    public String getColor() {
        return color;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        if (isMoving) {
            this.speed = speed;
        } else {
            System.out.println("Can't set speed!");
        }
    }


    public void start() {
        System.out.println("car.Car was started!");
        this.isMoving = true;
    }

    @Override
    public String toString() {
        return "car.Car{" +
                "brand='" + brand + '\'' +
                ", color='" + color + '\'' +
                ", speed=" + speed +
                ", isMoving=" + isMoving +
                ", entertainment=" + extras +
                '}';
    }
}
