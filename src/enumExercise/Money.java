package enumExercise;

public class Money {

    private double value;
    private String cur;

    public static void main(String[] args) {

        Money money = new Money();
        money.setCur("EUR");
        money.setValue(40.0);

        System.out.println(money);

        Money money1=Currency.exchange(money,"USD");
        System.out.println(money1);



    }


    @Override
    public String toString() {
        return (value + " "+ cur);
    }

    public double getValue() {
        return value;
    }

    public String getCur() {
        return cur;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public void setCur(String cur) {
        this.cur = cur;
    }
}

