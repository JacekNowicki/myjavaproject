package enumExercise;

public enum Operation {
    PLUS('+'),
    MINUS('-'),
    MULTIPLY('*'),
    DIVIDE('/');

    private char symbol;

    Operation(char symbol) {
        this.symbol = symbol;
    }

    public double calculate(double a, double b){
        double total=0.0;
        switch(getSymbol()){
            case '+': total=a+b; break;
            case '-': total=a-b; break;
            case '*': total=a*b; break;
            case '/': total=a/b; break;
            default:
                System.out.println("Wrong entry");
        }
        return total;
    }

    public static String SymbolToEnumName(char character){
        for (Operation element: Operation.values()){
            if ((element.getSymbol()==character)) {
            return element.toString();}
            }
        return "Wrong Character";
    }


    public char getSymbol() {
        return symbol;
    }
}


