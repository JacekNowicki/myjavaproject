package enumExercise;

public enum Currency {
    DOLAR("USD", 3.75),
    EURO("EUR", 4.29),
    FUNT("GBP", 4.93),
    JEN("JPY", 3.30),
    ZLOTY("PLN", 0);

    private String symbol;
    private double kursDoPLN;

    Currency(String symbol, double kurs) {
        this.symbol = symbol;
        this.kursDoPLN = kurs;
    }

    public static String symbolToEnumName(String symbolName) {
        for (Currency element : Currency.values()) {
            if (element.getSymbol() == symbolName) {
                return element.toString();
            }
        }
        return "Wrong symbol";
    }


    public static Money exchange(Money money,String toCurency){
         double inPLN;
        System.out.println(money.getCur());
        if (money.getCur()==toCurency) return money;
        if ((money.getCur()==DOLAR.getSymbol())) {
            inPLN= DOLAR.calculateCurrencyToPLN(money.getValue());
            money.setValue(fromPLN(inPLN,toCurency));
            money.setCur(toCurency);
            return money;
        }
        if ((money.getCur()==EURO.getSymbol())) {
            inPLN= EURO.calculateCurrencyToPLN(money.getValue());
            money.setValue(fromPLN(inPLN,toCurency));
            money.setCur(toCurency);
            return money;
        }
        if ((money.getCur()==FUNT.getSymbol())) {
            inPLN= FUNT.calculateCurrencyToPLN(money.getValue());
            money.setValue(fromPLN(inPLN,toCurency));
            money.setCur(toCurency);
            return money;
        }
        if ((money.getCur()==JEN.getSymbol())) {
            inPLN= JEN.calculateCurrencyToPLN(money.getValue());
            money.setValue(fromPLN(inPLN,toCurency));
            money.setCur(toCurency);
            return money;
        }

        return money;
    }

    public static double fromPLN (double amount, String cur1){
        if (cur1==DOLAR.getSymbol()){ return (amount/DOLAR.getKursDoPLN());}
        if (cur1==FUNT.getSymbol()){ return (amount/FUNT.getKursDoPLN());}
        if (cur1==EURO.getSymbol()){ return (amount/EURO.getKursDoPLN());}
        if (cur1==JEN.getSymbol()){ return (amount/JEN.getKursDoPLN());}

    return amount;
    }



    public double calculateCurrencyToPLN(double amount) {
        double result = 0.0;
        switch (getSymbol()) {
            case "USD": {
                result = amount * DOLAR.getKursDoPLN();
                break;
            }
            case "JPY": {
                result = amount * JEN.getKursDoPLN();
                break;
            }
            case "EUR": {
                result = amount * EURO.getKursDoPLN();
                break;
            }
            case "GBP": {
                result = amount * FUNT.getKursDoPLN();
                break;
            }
            default: {
                System.out.println("Wrong entry");
            }
        }
        return result;
    }

    public String getSymbol() {
        return symbol;
    }

    public double getKursDoPLN() {
        return kursDoPLN;
    }
}
