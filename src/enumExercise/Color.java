package enumExercise;

public enum Color {
    RED(1),WHITE(2),BLACK(3);

    int id;

    Color (int id){this.id=id;}

    public int getId() {
        return id;
    }
}
