package staticExercises;

import java.util.Calendar;

public class Product {

    private static int counter = 100;
    private int id;
    private String name;
    private double price;
    final double DISCOUNT = .3;
    private int dayOfWeek = Calendar.DAY_OF_WEEK;

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + calcPrice(price) + "; id= " + getId() +
                '}';
    }

    public double calcPrice(double price) {
        if (dayOfWeek == 1) return (price * DISCOUNT);
        return price;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
        this.id = counter;
        counter++;
    }
}
