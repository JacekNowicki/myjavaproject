package staticExercises;

public class TestStatic {

    public static void main(String[] args) {
        Product pro1 = new Product("mlotek", 300);
        Product pro2 = new Product("siekiera", 300);
        Product pro3 = new Product("pila", 50);

        System.out.println(pro1);
        System.out.println(pro2);
        System.out.println(pro3);

    }
}
