package staticExercises;

public class Math {
    private Math() {}

    static final double PI = 3.14;

    public static void main(String[] args) {

        double a = 6;
        double b = 3;
        int c = 3;

        System.out.println(Operation.add(a, b));
        System.out.println(Operation.subtract(a, b));
        System.out.println(Operation.multiply(a, b));
        System.out.println((Operation.divide(a, b)));
        System.out.println((Compare.min(a, b)));
        System.out.println(Compare.max(a, b));
        System.out.println(pow(a, c));
        System.out.println(poleKola(8));

    }

    static class Operation {

        public static double add(double a, double b) {
            return (a + b);
        }

        public static double subtract(double a, double b) {
            return (a - b);
        }

        public static double multiply(double a, double b) {
            return (a * b);
        }

        public static double divide(double a, double b) {
            return (a / b);
        }

    }

    static class Compare {

        public static double min(double a, double b) {
            if (a < b) return a;
            return (b);
        }

        public static double max(double a, double b) {
            if (a > b) return a;
            return (b);
        }
    }

    public static double pow(double a, int b) {
        double total = a;
        for (int i = 1; i < b; i++) {
            total = total * a;
        }
        return total;

    }

    public static double poleKola(double r) {
        return (PI * r * r);
    }


}
