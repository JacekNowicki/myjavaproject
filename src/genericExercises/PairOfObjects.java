package genericExercises;

public class PairOfObjects<T1 extends String, T2 extends Number> {

    private T1 object1;
    private T2 object2;

    public PairOfObjects(T1 object1, T2 object2) {
        this.object1 = object1;
        this.object2 = object2;
    }

    public T1 getObject1() {
        return object1;
    }

    public T2 getObject2() {
        return object2;
    }

    public static void main(String[] args) {

        String sentence = "Hello World";
        Double number = 1000.679;

        PairOfObjects pair = new PairOfObjects(sentence, number);

        System.out.println(pair.getObject1());
        System.out.println(pair.getObject2());


    }

}
