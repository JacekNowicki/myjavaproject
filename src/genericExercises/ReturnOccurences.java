package genericExercises;

import java.util.HashSet;

public class ReturnOccurences {

    public static void main(String[] args) {
        String word = "jestem";
        HashSet<String> col = new HashSet<>();
        col.add("nie jestem nie nie");
        col.add("Nie poniewaz nie");
        col.add("raczej nie jestem nie");

        System.out.println("Liczba wystapien wyrazu \"" + word + "\" to: "+checkOccure(col, word));

    }

    public static int checkOccure(HashSet<String> collection, String word) {
        int number = 0;

        for (String element : collection) {
            if (element.contains(word)) {

                String[] tab = (element.split(" "));

                for (String element2 : tab) {
                    if (element2.toLowerCase().equals(word)) {
                        number++;
                    }

                }
            }
        }
        return number;
    }
}