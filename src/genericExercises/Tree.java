package genericExercises;

public interface Tree {

    String getName();
    int getAge();
}
