package genericExercises;

import java.util.HashMap;

public class MapOfWords {

    private HashMap<String, Integer> mapa;
    private static int key = 0;

    public MapOfWords() {
        mapa = new HashMap<>();
    }

    public static void main(String[] args) {

        MapOfWords mapOfWords = new MapOfWords();

        mapOfWords.addWord("Jacek");
        mapOfWords.addWord("Agata");
        mapOfWords.addWord("Jessica");

        System.out.println(mapOfWords.getWordCount());

    }

    private int getWordCount() {
        return key;
    }

    private void addWord(String word) {
        this.mapa.put(word, key);
        key++;
    }

}
