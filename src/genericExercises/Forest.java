package genericExercises;

import java.util.ArrayList;
import java.util.List;

public class Forest<T extends Tree> {

//    private T drzewko;

  //  public Forest(T t) {
  //      this.drzewko = t;
  //  }

    public static void main(String[] args) {

        List<Tree> forest = new ArrayList<>();


        forest.add(new Coniferous("jodla", 50));
        forest.add(new Coniferous("sosna", 80));
        forest.add(new Leafy("brzoza", 140));


        for (Tree element : forest) {
            if (element instanceof Leafy) {
                System.out.println("Lisciaste: " + element.getName());
            }
        }
        for (Tree element : forest) {
            if (element instanceof Coniferous) {
                System.out.println("Iglaste: " + element.getName());
            }
        }

        for (Tree element : forest) {
            if (element.getAge() > 60) {
                System.out.println("Powyzej 60: " + element.getName() + " " + element.getAge());
            }
        }

    }

}
