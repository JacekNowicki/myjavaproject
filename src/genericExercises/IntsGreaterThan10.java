package genericExercises;

import java.util.ArrayList;
import java.util.List;

public class IntsGreaterThan10 {

    public static void main(String[] args) {

        List<Double> integerList = new ArrayList<>();
        integerList.add(50.1);
        integerList.add(10.1);
        integerList.add(15.5);
        integerList.add(20.0);
        integerList.add(25.4);

        checkIfOverTen(integerList);

    }

    private static void checkIfOverTen(List<? extends Number> list) {
        for (Number element : list) {
            if (element.doubleValue() > 10) {
                System.out.println(element);
            }
        }

    }
//    private static <T extends Number> void checkIfOverTen(List<T> list) {
//        for (Number element : list) {
//            if (element.doubleValue() > 10) {
//                System.out.println(element);
//            }
//        }
//
//    }


}
