package temp2;

import temp.FahrenheitConverter;

public class Temperature {
    private int temperature;
    private String date;
    private String hour;

    public Temperature(int temperature, String date, String hour) {
        this.temperature = temperature;
        this.date = date;
        this.hour = hour;
    }

    public static void main(String[] args) {
        temp.Temperature temp = new temp.Temperature(13, "2018-10-1", "10:45");
        temp.show();
        temp.showInFarenheit();
    }

    public void showInFarenheit() {
        int x;
        x = (int)FahrenheitConverter.fromCelciusConverter(getTemperature());
        System.out.println(getDate() + " " + getHour() + " " + "- " + x + "\u00b0F");
    }

    public void show() {
        System.out.println(getDate() + " " + getHour() + " " + "- " + getTemperature() + "\u00b0C");
    }

    public int getTemperature() {
        return temperature;
    }

    public String getDate() {
        return date;
    }

    public String getHour() {
        return hour;
    }

}
