package temp2;

import java.util.Scanner;

public class FahrenheitConverter {
    public static void main(String[] args) {
        Scanner text = new Scanner(System.in);
        double c, f;
        char entryChar;
        String entryString;

        System.out.println("Program konwertuje temperature z C\u00b0 na F\u00b0 i z F\u00b0 na C\u00b0");
        System.out.println("Podajesz stopnie Farenheita, czy Celcjusza?");
        System.out.println("F - z farnheit na celcjus , C - z celcius na farenheit");
        entryString = text.next();
        entryChar = entryString.charAt(0);

        switch (entryChar) {
            case 'f': {
            }
            case 'F': {
                System.out.println("Wpisz stopnie do konwersji z Farenheit do Celcius:");
                f = text.nextDouble();
                c = fromFahrenheitConverter(f);
                System.out.printf("%.0fF\u00b0 to %.0fC\u00b0", f, c);
                break;
            }
            case 'c': {
            }
            case 'C': {
                System.out.println("Wpisz stopnie do konwersji z Farenheit do Celcius:");
                c = text.nextDouble();
                f = fromCelciusConverter(c);
                System.out.printf("%.0fC\u00b0 to %.0fF\u00b0", c, f);
                break;
            }
            default:
                System.out.println("nalezy wprowadzic \"f\" lub \"c\"");
        }

    }

    public static double fromFahrenheitConverter(double f) {
        double c;
        c = (f - 32) * 0.5556;
        return c;
    }

    public static double fromCelciusConverter(double c) {
        double f;
        f = c * (9 / 5) + 32;
        return f;
    }
}
