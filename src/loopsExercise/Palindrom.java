package loopsExercise;

import javax.swing.*;

public class Palindrom {

    public static void main(String[] args) {
        String text = JOptionPane.showInputDialog("Wpisz tekst: ");
        boolean isPalindrom = false;
        text = text.replace(" ", "");

        for (int i = 0; i < (text.length() - 1); i++) {
            if (text.charAt(i) == (text.charAt(text.length() - i - 1))) {
                isPalindrom = true;
            } else {
                isPalindrom = false;
                break;
            }
        }

        if (isPalindrom == true) {
            System.out.println("Text jest Palindromem");
        } else {
            System.out.println("Text NIE jest Palindromem");
        }
    }
}
