package loopsExercise;

public class PhraseInText {

    public static void main(String[] args) {

        String phrase, text;
        text = "Kot. Ala ma kota a kot ma Ale. Adas kot ma rowniez kota. Jurek tez ma kota, ale Krystian juz kota nie ma. Niech zyje kot";
        phrase = "kot";
        PhraseInText find = new PhraseInText();
        System.out.println("Liczba fraz " + "\"" + phrase + "\"" + " w tekscie to: " + find.numberOfPhrasesInText(phrase, text));
        //String text1=text.toLowerCase();
    }
    private int numberOfPhrasesInText(String phrase, String text1) {
        int number = 0;
        int index = 0;
        for (int i = 0; i < (text1.length()); i++) {
            if (text1.indexOf(phrase, index) >= 0) {
                number++;
                index = text1.indexOf(phrase, index) + 1;
            }
        }
        return number;
    }
}