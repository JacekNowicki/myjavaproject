package loopsExercise;

public class AlphLac {

    public static void main(String[] args) {

        char character;

        for (int i=65; i<=90; i=i+2){
            character=(char) i;
            System.out.print(character+" ");
        }
        System.out.println();
        int i = 65;
        while (i<=90){
            character=(char) i;
            System.out.print(character+" ");
            i=i+2;
        }

    }


}
