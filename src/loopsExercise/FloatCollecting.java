package loopsExercise;

import javax.swing.*;

public class FloatCollecting {

    public static void main(String[] args) {

        String entry1 = JOptionPane.showInputDialog("Ile liczb chcesz wpisac?");
        int n = Integer.valueOf(entry1);
        float[] tab = new float[n];
        float sum = 0;

        for (int i = 0; i < n; i++) {
            String entry = JOptionPane.showInputDialog("Wpisz liczbe nr " + (i + 1) + ":");
            tab[i] = Float.valueOf(entry);
            sum = sum + tab[i];
        }

        System.out.println("Wpisales " + n + " liczb.");
        System.out.println("Suma liczb: " + sum);
        System.out.printf("Srednia arytmetyczna liczb: %.2f", sum / n);

    }

}
