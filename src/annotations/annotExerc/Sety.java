package annotations.annotExerc;

import java.util.*;

public class Sety {

    public static void main(String[] args) {

        HashSet<String> zbior = addString("Ala", "ma", "kota");

        for (String element : zbior) {
            System.out.println(element);
        }

        List<String> lista = new ArrayList<>();
        lista.addAll(zbior);
        Collections.sort(lista);

        if (lista.contains("Ala")) lista.remove("pies");

        for (String element:lista){
            System.out.println(element);
        }
    }

    private static HashSet<String> addString (String... str){

        HashSet<String> zbior = new HashSet<>();
        zbior.add("pies");

        zbior.addAll(Arrays.asList(str));
        return zbior;
    }
}
