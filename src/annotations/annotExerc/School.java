package annotations.annotExerc;

@Info(
        author = "JacekNow",
        date = "10-12-2020",
        description = "example info about School class"
)
public class School {

    private final String name;

    public School(String name) {
        this.name = name;
    }

    @Info(
            date = "10-10-2018",
            description = "example infor aout get school name method"
    )
    public String getName() {
        return name;
    }
}


