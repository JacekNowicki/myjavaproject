package annotations.annotExerc;

@Info(
        author = "JacekN",
        date = "10-14-2019",
        description = "info about student class"
)
public class Student {
    private String firstName;
    private String lastName;
    private int age;

    public Student(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public Student() {
    }
    @Info(
            date = "10-10-2018",
            description = "example info about student getFirstName method"
    )
    public String getFirstName() {
        return firstName;
    }
    @Info(
            date = "10-11-2018",
            description = "example info about student getLastName method"
    )
    public String getLastName() {
        return lastName;
    }
    @Info(
            date = "10-13-2018",
            description = "example info about get age method"
    )
    public int getAge() {
        return age;
    }
}
