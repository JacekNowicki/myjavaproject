package annotations.annotExerc;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Sety2 {

    public static void main(String[] args) {

Stack<String> stack = new Stack<>();
stack.push("Hello");
stack.push("my");
stack.push("world");

        System.out.println(stack.peek());
        stack.pop();
        System.out.println(stack.peek());




//        FizzBuzz();
//        System.out.println();
//        Factor(10);
//        System.out.println();
//        System.out.println(Factor2(10));
//        System.out.println(Factor3(9));
//        System.out.println();
//        System.out.println(sumOfIntString(3456));
//        System.out.println();
//        System.out.println();
//        int x = 7;
//        System.out.printf("Jest %d%n liczba pierwsza?: " + liczbaPierwsza(x), x);
//        System.out.println();
//        pickWordsForB("Ala ma kota, ale nie ma psa.");


   /*     String[] zbior = {"Ala", "ma", "kota", "ala", "kota"};

//        for (String element:zbior){
//            element= element.toLowerCase();
//            System.out.println("elem "+element);
//        }

        Stream<String> stream = Arrays.asList(zbior).stream();

        List<String> lista = stream
                .map(x -> x.toLowerCase())
                .collect(Collectors.toList());

        //  String[] zbior2 =
        lista.toArray(zbior);

        // System.out.println(zbior2);

        System.out.println(numbOfDistinct(zbior));
        int x = numbOfDistinct(zbior);

        switch (x) {
            case 4:
                System.out.println("jest 4");
                break;
            default:
                System.out.println(x);
*/
        //     }


    }


    private static int numbOfDistinct(Object[] tab) {

        Set<Object> set = new HashSet<>();

        set.addAll(Arrays.asList(tab));
        System.out.println(set);
        int count = set.size();

        return count;
    }

    private static int sumOfIntString(int x) {
        int sum = 0;
        String string = Integer.toString(x);
        for (int i = 0; i < string.length(); i++) {
            sum = sum + Character.getNumericValue(string.charAt(i));
        }
        return sum;

    }

    private static void FizzBuzz() {

        for (int i = 1; i <= 100; i++) {
            if ((i % 3 == 0) && (i % 5 == 0)) System.out.print(" FizzBuzz ");
            else if (i % 3 == 0) System.out.print(" Fizz ");
            else if (i % 5 == 0) System.out.print(" Buzz ");
            else System.out.print(" " + i + " ");
        }

    }

    private static void Factor(int x) {
        int sum = 1;
        for (int i = 1; i <= x; i++) {
            sum = sum * i;
        }
        System.out.println(sum);
    }

    private static int Factor3(int x) {
        if (x < 2) return 1;

        return x * Factor3(x - 1);
    }


    private static int Factor2(int x) {
        if (x < 2) return 1;

        return x * Factor2(x - 1);
    }

    private static boolean liczbaPierwsza(int x) {
        for (int i = 2; i < x; i++) {
            if (x % i == 0) return false;
        }
        return true;
    }

    private static void pickWordsForB (String text){

        String tekstPure = text.replace(",","").replace(".","");
        String[] words = tekstPure.split(" ");
        List<String> pickedWords = new ArrayList<>();

        for (int i=0; i<words.length; i++){
            if (words[i].toLowerCase().charAt(0)=='a') pickedWords.add(words[i]);
        }
        System.out.println("tekst: "+ text);
        System.out.println("tekst bez interpunkcji: "+ tekstPure);
        System.out.println("Words for A: ");
        for (String element:pickedWords){
            System.out.println(element);
        }

    }

}
