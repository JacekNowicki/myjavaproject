package dateTimeExercises;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;

import static java.lang.Math.round;

public class Ex6 {

    public static void main(String[] args) {

//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy, h:mma E");
//        String now = "22-Feb-1979, 06:00PM Thu";

        LocalDateTime myBirthday = LocalDateTime.of(2018,12,16 ,18,0,0);
        LocalDateTime current = LocalDateTime.now();
        LocalDateTime justTime = LocalDateTime.of(current.getYear(),current.getMonth(),current.getDayOfMonth(),
                myBirthday.getHour(),myBirthday.getMinute(),myBirthday.getSecond());

        Period period = Period.between(myBirthday.toLocalDate(),current.toLocalDate());

        Duration durationTime = Duration.between(justTime,current);
        long hoursOnly = round(durationTime.getSeconds()/3600);

        Duration durationTotal = Duration.between(myBirthday,current);
        long hoursTotal = round(durationTotal.getSeconds()/3600);

        System.out.println("Total hours: " +hoursTotal);
        System.out.println();
        System.out.println(period);

        System.out.println("Years "+period.getYears());
        System.out.println("Months "+period.getMonths());
        System.out.println("Days " +period.getDays());
        System.out.println("Hours " +hoursOnly);

        System.out.println(myBirthday.compareTo(current));

    }

}
