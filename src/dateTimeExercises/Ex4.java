package dateTimeExercises;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Ex4 {

    public static void main(String[] args) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy, h:mma E");
        String now = "22-Feb-1979, 06:00PM Thu";

        LocalDateTime localDateTime = LocalDateTime.parse(now, formatter);

        ZoneId zoneId = ZoneId.of("Europe/Warsaw");
        ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, zoneId);
        System.out.println(zonedDateTime.withZoneSameInstant(zoneId));

        ZoneId zoneJap = ZoneId.of("Asia/Tokyo");
        System.out.println(zonedDateTime.withZoneSameInstant(zoneJap));


    }

}
