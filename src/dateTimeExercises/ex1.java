package dateTimeExercises;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class ex1 {

    public static void main(String[] args) {

        Calendar kalendarz = Calendar.getInstance();
        kalendarz.set(1979, 1, 22);
       // Date data = kalendarz.getTime();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMMM dd, yyyy zz hh:mm");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("HST"));
        System.out.println(simpleDateFormat.format(kalendarz.getTime()));


        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("MMMMM, EE dd, yyyy zz hh:mm");
        simpleDateFormat2.setTimeZone(TimeZone.getTimeZone("GMT"));

        System.out.println(simpleDateFormat2.format(kalendarz.getTime()));


    }

}
