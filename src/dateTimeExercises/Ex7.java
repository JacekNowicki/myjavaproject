package dateTimeExercises;

import java.time.LocalDateTime;

public class Ex7 {

    public static void main(String[] args) {
        for (int yob = 2019; yob < 2030; yob++) {
            LocalDateTime myBirthday = LocalDateTime.of(yob, 02, 22, 0, 0, 0);
            System.out.println("Rok " + yob + ": " + myBirthday.getDayOfWeek());
        }

        // LOCALTIMEDATE implements COMPARABLE  !!!!!!!!!!

    }
}
