package task;

import task.items.ItemManager;
import task.items.ItemModel;
import task.user.UserManager;

import static task.items.ItemUtil.truncate;

public class OnlineShop {
    public static void main(String[] args) {

        UserManager person = new UserManager();
        person.createUser("Jacek","Nowicki",39);
        System.out.println(person);


        String trimmed = truncate("Czarodziej biega i zabija",16);
        ItemManager item = new ItemManager();
        ItemModel item1 = item.createShopItem("Potter",trimmed,99.99);
        System.out.println(item1);
    }
}
