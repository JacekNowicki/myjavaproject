package task.user;

public class UserManager {

    protected UserModel user = new UserModel();

    public void createUser(String firstName, String lastName, int age) {
        this.user.setFirstName(firstName);
        this.user.setLastName(lastName);
        this.user.setAge(age);
    }

    @Override
    public String toString() {
        return "UserManager{" +
                "user=" + user +
                '}';
    }

    protected UserModel getUser() {
        return user;
    }
}
