package family;

class Person implements Comparable<Person>{
    private String firstName;
    private String lastName;
    private int age;

    protected Person(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    @Override
//    public String toString() {
//        return "Person{" +
//                "firstName='" + firstName + '\'' +
//                ", lastName='" + lastName + '\'' +
//                ", age=" + age +
//                '}';
//    }
    public String toString(){
        //String text = ;

        return (" "+firstName+" "+lastName+", lat: "+age);
    }

    protected String getFirstName() { return firstName;}

    protected String getLastName() {
        return lastName;
    }

    protected int getAge() {
        return age;
    }


    @Override
    public int compareTo(Person o) {
        return this.getLastName().compareTo(o.getLastName());
    }
}
