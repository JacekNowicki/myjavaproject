package family;

class Family {
    private Person son, daughter,mother, father, motherMother,motherFather, fatherMother,fatherFather;

    protected Family(Person son, Person daughter, Person mother,
                  Person father, Person motherMother, Person motherFather,
                  Person fatherMother, Person fatherFather) {
        this.son = son;
        this.daughter = daughter;
        this.mother = mother;
        this.father = father;
        this.motherMother = motherMother;
        this.motherFather = motherFather;
        this.fatherMother = fatherMother;
        this.fatherFather = fatherFather;
    }

     protected int FamilyAgeSum(){
        return (son.getAge()+daughter.getAge()+mother.getAge()+father.getAge()+motherMother.getAge()
                +motherFather.getAge()+fatherFather.getAge()+fatherMother.getAge());
     }

     protected double FamilyAgeAv(){
        return (FamilyAgeSum()/8);
     }


    @Override
    public String toString() {
//        return "Family{" +
//                "\nson=" + son +
//                ", \ndaughter=" + daughter +
//                ", \nmother=" + mother +
//                ", \nfather=" + father +
//                ", \nmotherMother=" + motherMother +
//                ", \nmotherFather=" + motherFather +
//                ", \nfatherMother=" + fatherMother +
//                ", \nfatherFather=" + fatherFather +
//                '}';
    return("Syn: "+son+"\n"+"Corka: "+daughter+"\n"+"Matka: "+mother+"\n"+"Ojciec: "+father+"\n"
    +"Ojciec Matki: "+motherFather+"\n"+"Matka matki: "+motherMother+"\n"+"Ojciec ojca: "+fatherFather+"\n"
    +"Matka ojca: "+fatherMother+"\n");
    }


    protected Person getSon() {
        return son;
    }

    protected Person getDaughter() {
        return daughter;
    }

    protected Person getMother() {
        return mother;
    }

    protected Person getFather() {
        return father;
    }

    protected Person getMotherMother() {
        return motherMother;
    }

    protected Person getMotherFather() {
        return motherFather;
    }

    protected Person getFatherMother() {
        return fatherMother;
    }

    protected Person getFatherFather() {
        return fatherFather;
    }


}
