package family;

import java.util.Collections;
import java.util.Comparator;

class FamilyTest {

    public static void main(String[] args) {
        Person son = new Person("Jacek", "Nowicki",39);
        Person daughter = new Person("Marta","Nowicki",46);
        Person mother = new Person("Teresa","Nowicka",63);
        Person father = new Person("Jozef","Nowicki",66);
        Person motherMother = new Person("Wanda","Porczynska",69);
        Person motherFather = new Person("Antoni","Porczynski",93);
        Person fatherMother = new Person("Feliksa","Nowicka",90);
        Person fatherFather = new Person("Stanislaw","Nowicki",99);

        Family Nowicki = new Family(son,daughter,mother,father,motherMother,motherFather,fatherMother,fatherFather);

        Comparator <Person> nazwisko = Comparator.comparing(Person::getAge);

        //Collections.sort(Nowicki,nazwisko);

        System.out.println(Nowicki);
        System.out.println("\nSuma lat wszystkich czlonkow rodziny: "+Nowicki.FamilyAgeSum());
        System.out.println("\nSrednia lat wszystkich czlonkow rodziny: "+Nowicki.FamilyAgeAv());
    }

}
