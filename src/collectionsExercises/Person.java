package collectionsExercises;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Person implements Comparable<Person>{
    private String firstName;
    private String lastName;

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public int compareTo(Person person) {

        if (this.lastName.compareTo(person.getLastName())==0){
            return  (this.firstName.compareTo(person.getFirstName()));
        }
        else return this.lastName.compareTo(person.getLastName());
    }
    public static void main(String[] args) {

        List<Person> personList = new ArrayList<>();
        personList.add(new Person("Jacek","Nowicki"));
        personList.add(new Person("Jacek","Nawicki"));
        personList.add(new Person("Jocek","Nawicki"));
        personList.add(new Person("Aocek","Nawicki"));
        personList.add(new Person("Jocek","Mawicki"));

        Collections.sort(personList);

        Comparator<Person> byFirstName = Comparator.comparing(Person::getFirstName);
        Collections.sort(personList,byFirstName);

        for (Person per:personList) {
            System.out.println(per);
        }
    }

    @Override
    public String toString() {
        return "Person{" +
                ", lastName='" + lastName + '\'' +
                "firstName='" + firstName + '\'' +
                '}';
    }
}
