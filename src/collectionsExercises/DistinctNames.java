package collectionsExercises;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class DistinctNames {

    public static void main(String[] args) {
        Set<String> zbior= new HashSet<>();

        Scanner entry = new Scanner(System.in);
        String text="";

        do {
            System.out.print("Wpisz imie: [ 'q' - wyjscie ] ");
            text = entry.next();
            if(!text.equalsIgnoreCase("q")){zbior.add(text);}

        }
        while (!text.equalsIgnoreCase("q"));

        System.out.println("Wpisane imiona: ");
        for (String element : zbior) {
            System.out.println(element);
        }
    }

    }

