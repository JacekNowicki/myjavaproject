package collectionsExercises.collectionCar;

import java.util.*;

public class Car implements Comparable<Car> {
    private final String name;
    private final String color;
    private int doors;

    public Car(String name, String color, int doors) {
        this.name = name;
        this.color = color;
        this.doors = doors;

    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public int getDoors() {
        return doors;
    }

    @Override
    public int compareTo(Car o) {
        return this.name.compareTo(o.getName());
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", doors=" + doors +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(name, car.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public static void main(String[] args) {
        List<Car> cars = new ArrayList<>();


        cars.add(new Car("Tesla Model S", "black", 4));
        cars.add(new Car("Tesla Model X", "blue", 6));
        cars.add(new Car("Ford Escort", "green", 4));
        cars.add(new Car("Ford Focus", "red", 4));
        cars.add(new Car("BMW X3", "grey", 5));
        cars.add(new Car("BMW X1", "yellow", 3));
        cars.add(new Car("BMW X5", "purple", 4));
        cars.add(new Car("Fiat Tipo", "purple", 4));
        cars.add(new Car("Fiat Tempra", "purple", 6));
        cars.add(new Car("Fiat 126p", "purple", 7));


        List<Car> choosenCars = new ArrayList<>();
        Scanner text = new Scanner(System.in);
        System.out.println("Wpisz kolor: ");
        String col = text.next();
        System.out.println("Wpisz drzwi: ");
        int dor = text.nextInt();

        System.out.println(col);
        System.out.println(dor);

        dor = 4;
        col = "purple";

        System.out.println();

        for (Car element : cars) {
            if ((element.getDoors() == dor) && (element.getColor() == col)) {
                choosenCars.add(element);
            }
        }

        System.out.println("Dostepne auta:");
        for (Car element : choosenCars) {
            System.out.println(element.getName());
        }

        System.out.println();
        System.out.println();

        choosenCars.clear();

            Iterator<Car> iterator = cars.iterator();
            while (iterator.hasNext()) {
                iterator.next().setDoors(4);
            }



        for (Car element : cars) {
                if ((element.getDoors() == dor) && (element.getColor() == col)) {
                    choosenCars.add(element);
                }
            }
        System.out.println("Dostepne auta:");
        for (Car element : choosenCars) {
            System.out.println(element.getName());
        }


    }
}
