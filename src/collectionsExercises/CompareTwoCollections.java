package collectionsExercises;

import java.util.ArrayList;
import java.util.List;

public class CompareTwoCollections {

    public static void main(String[] args) {

        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>();
        List<Integer> list1distinct;
        List<Integer> list2distinct;
        List<Integer> commonItems;

        list1.add(1);
        list1.add(3);
        list1.add(5);
        list1.add(7);
        list2.add(2);
        list2.add(3);
        list2.add(4);
        list2.add(5);

        commonItems = CommonElements(list1, list2);
        list1distinct = list1;
        list2distinct = list2;

        list1distinct.removeAll(commonItems);
        list2distinct.removeAll(commonItems);

        System.out.println("Common items: " + commonItems);
        System.out.println("List 1 distinct items: " + list1distinct);
        System.out.println("List 2 distinct items: " + list2distinct);

    }

    public static List<Integer> CommonElements(List<Integer> list1, List<Integer> list2) {

        List<Integer> tab_operation = new ArrayList<>();

        for (Integer element : list1) {
            if (list2.contains(element)) {
                tab_operation.add(element);
            }
        }
        return tab_operation;

    }


}
