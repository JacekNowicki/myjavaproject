package javaFXexercises;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Calculator extends Application {

    private Stage window;
    private String display = "0";
    private long x = 0;
    private long y = 0;
    private String sign = " ";

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        window = primaryStage;
        window.setTitle("Calculator");

        GridPane grid = new GridPane();
        grid.setGridLinesVisible(false);
        grid.setAlignment(Pos.CENTER);
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(8);
        grid.setHgap(8);

        Button button1 = new Button("1");
        button1.setMinSize(30.0, 30.0);
        Button button2 = new Button("2");
        button2.setMinSize(30.0, 30.0);
        Button button3 = new Button("3");
        button3.setMinSize(30.0, 30.0);
        Button button4 = new Button("4");
        button4.setMinSize(30.0, 30.0);
        Button button5 = new Button("5");
        button5.setMinSize(30.0, 30.0);
        Button button6 = new Button("6");
        button6.setMinSize(30.0, 30.0);
        Button button7 = new Button("7");
        button7.setMinSize(30.0, 30.0);
        Button button8 = new Button("8");
        button8.setMinSize(30.0, 30.0);
        Button button9 = new Button("9");
        button9.setMinSize(30.0, 30.0);
        Button button0 = new Button("0");
        button0.setMinSize(30.0, 30.0);
        Button buttonMin = new Button("-");
        buttonMin.setMinSize(30.0, 30.0);
        Button buttonPlus = new Button("+");
        buttonPlus.setMinSize(30.0, 30.0);
        Button buttonMultiply = new Button("*");
        buttonMultiply.setMinSize(30.0, 30.0);
        Button buttonDivide = new Button("/");
        buttonDivide.setMinSize(30.0, 30.0);
        Button buttonEquals = new Button("=");
        buttonEquals.setMinSize(30.0, 30.0);
        Button buttonClr = new Button("Clr");
        buttonClr.setMinSize(30.0, 30.0);

        GridPane.setConstraints(button1, 0, 1);
        GridPane.setConstraints(button2, 1, 1);
        GridPane.setConstraints(button3, 2, 1);
        GridPane.setConstraints(buttonPlus, 3, 1);
        GridPane.setConstraints(button4, 0, 2);
        GridPane.setConstraints(button5, 1, 2);
        GridPane.setConstraints(button6, 2, 2);
        GridPane.setConstraints(buttonMin, 3, 2);
        GridPane.setConstraints(button7, 0, 3);
        GridPane.setConstraints(button8, 1, 3);
        GridPane.setConstraints(button9, 2, 3);
        GridPane.setConstraints(buttonMultiply, 3, 3);
        GridPane.setConstraints(button0, 0, 4);
        GridPane.setConstraints(buttonClr, 1, 4);
        GridPane.setConstraints(buttonEquals, 2, 4);
        GridPane.setConstraints(buttonDivide, 3, 4);
        Label label = new Label("digits");
        Label label2 = new Label("x,y");

        grid.getChildren().addAll(button0, button1, button2, button3, button4, button5, button6, buttonClr,
                button7, button8, button9, buttonDivide, buttonEquals, buttonMin, buttonMultiply, buttonPlus);

        HBox dspl = new HBox();
        dspl.getChildren().add(label);

        BorderPane borderPane = new BorderPane();
        borderPane.setTop(dspl);
        borderPane.setBottom(label2);
        borderPane.setCenter(grid);


        buttonClr.setOnAction(e -> {
            display = "0";
            x = 0;
            y = 0;
            sign = "";
            label.setText(display);
            label2.setText("x=" + x + " " + sign + " " + "y=" + y);

        });
        button0.setOnAction(e -> {
            addDigit("0");
            label.setText(display);
            label2.setText("x=" + x + " " + sign + " " + "y=" + y);

        });
        button1.setOnAction(e -> {
            addDigit("1");
            label.setText(display);
            label2.setText("x=" + x + " " + sign + " " + "y=" + y);

        });
        button2.setOnAction(e -> {
            addDigit("2");
            label.setText(display);
            label2.setText("x=" + x + " " + sign + " " + "y=" + y);

        });
        button3.setOnAction(e -> {
            addDigit("3");
            label.setText(display);
            label2.setText("x=" + x + " " + sign + " " + "y=" + y);

        });
        button4.setOnAction(e -> {
            addDigit("4");
            label.setText(display);
            label2.setText("x=" + x + " " + sign + " " + "y=" + y);

        });
        button5.setOnAction(e -> {
            addDigit("5");
            label.setText(display);
            label2.setText("x=" + x + " " + sign + " " + "y=" + y);

        });
        button6.setOnAction(e -> {
            addDigit("6");
            label.setText(display);
            label2.setText("x=" + x + " " + sign + " " + "y=" + y);

        });
        button7.setOnAction(e -> {
            addDigit("7");
            label.setText(display);
            label2.setText("x=" + x + " " + sign + " " + "y=" + y);

        });
        button8.setOnAction(e -> {
            addDigit("8");
            label.setText(display);
            label2.setText("x=" + x + " " + sign + " " + "y=" + y);

        });
        button9.setOnAction(e -> {
            addDigit("9");
            label.setText(display);
            label2.setText("x=" + x + " " + sign + " " + "y=" + y);
        });

        buttonPlus.setOnAction(e -> {
            y = 0;
            if (!sign.equals("+")) {
                x = Long.valueOf(display);
            }
            sign = "+";
            display = "0";
            label.setText(display);
            label2.setText("x=" + x + " " + sign + " " + "y=" + y);
        });

        buttonMin.setOnAction(e -> {
            y = 0;
            if (!sign.equals("-")) {
                x = Long.valueOf(display);
            }
            sign = "-";
            display = "0";
            label.setText(display);
            label2.setText("x=" + x + " " + sign + " " + "y=" + y);
        });
        buttonMultiply.setOnAction(e -> {
            y = 0;
            if (!sign.equals("*")) {
                x = Long.valueOf(display);
            }
            sign = "*";
            display = "0";
            label.setText(display);
            label2.setText("x=" + x + " " + sign + " " + "y=" + y);
        });
        buttonDivide.setOnAction(e -> {
            y = 0;
            if (!sign.equals("/")) {
                x = Long.valueOf(display);
            }
            sign = "/";
            display = "0";
            label.setText(display);
            label2.setText("x=" + x + " " + sign + " " + "y=" + y);
        });
        buttonEquals.setOnAction(e -> {
            equal();
            label.setText(display);
            label2.setText("x=" + x + " " + sign + " " + "y=" + y);

        });

        Scene scene = new Scene(borderPane, 300, 300);
        window.setScene(scene);
        window.show();

    }

    private void addDigit(String digit) {
        if (display.equals("0")) display = "";
        display = display + digit;
    }


    private void equal() throws NumberFormatException {

        y = Long.valueOf(display);
        if (sign.equals("+")) {
            display = String.valueOf(x + y);
        } else if (sign.equals("-")) {
            display = String.valueOf(x - y);
        } else if (sign.equals("*")) {
            display = String.valueOf(x * y);
        } else if (sign.equals("/")) {
            display = String.valueOf(x / y);
        } else display = String.valueOf(x);

    }

}
