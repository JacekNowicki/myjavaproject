package javaFXexercises;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class AnotherWindow {

    private int textBack=0;

    public void otherWindow(String title){
        Stage window = new Stage();

        window.setTitle(title);
        VBox box = new VBox();
        Button button = new Button("CLOSE");
        Button button1 = new Button("add");
        button.setOnAction(e->window.close());
        button1.setOnAction(e->{
            textBack+=100;
            System.out.println(textBack);
            ExcersiseJavaFx.getDao().setX(textBack);
        });

        box.getChildren().addAll(button,button1);
        Scene scene = new Scene(box);

        window.setScene(scene);
        window.show();
    }
}
