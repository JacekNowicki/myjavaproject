package javaFXexercises;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ExA extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        Button button1 = new Button("minus");
        Button button2 = new Button("plus");
        TextField text1 = new TextField("");
        TextField text2 = new TextField("");

        Text text = new Text("...");

        Label label = new Label("wynikiem jest");

        VBox vBox = new VBox();

        vBox.getChildren().addAll(button1,button2,text1,text2,label,text);

        primaryStage.setTitle("Dodawanie i odejmowanie");
        primaryStage.setScene(new Scene(vBox,200,200));
        primaryStage.show();


        button1.setOnAction(e-> {
            int x = Integer.parseInt(text1.getText());
            int y = Integer.parseInt(text2.getText());
            text.setText(String.valueOf(x-y));
        });

        button2.setOnAction(e->{
            int x= Integer.parseInt(text1.getText());
            int y= Integer.parseInt(text2.getText());
            text.setText((String.valueOf(x+y)));
        });


    }
}
