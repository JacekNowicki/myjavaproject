package javaFXexercises;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ExcersiseJavaFx extends Application {
    Stage window;
    String login="";
    String password="";
    int temp;

    private static ClassDAO dao = new ClassDAO();

    public static ClassDAO getDao() {
        return dao;
    }

    public static void main(String[] args) {
        launch();
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;

        window.setTitle("Main Window");

        VBox box1 = new VBox();
        VBox box2 = new VBox();

        Button exit = new Button("Close");
        Button loginScene = new Button("Log in view");
        Button goBackToScene1 = new Button("go back to MAIN scene");
        Button acceptEntry  = new Button("LOG IN");

        Button openNewWindow = new Button("Open new Window");


        Label labelLogin = new Label("LogIn");
        Label labelPasword = new Label("Password");
        TextField textLogin = new TextField();
        textLogin.setPromptText("enter Login");
        TextField textPassword = new TextField();
        textPassword.setPromptText("enter password");

        box1.getChildren().addAll(exit,loginScene,openNewWindow);
        box2.getChildren().addAll(labelLogin,textLogin,labelPasword,textPassword, acceptEntry,goBackToScene1);

        Scene sceneFirst = new Scene(box1,200,200);
        Scene sceneSecond = new Scene(box2,500,500);

        window.setScene(sceneFirst);

        exit.setOnAction(e->window.close());
        loginScene.setOnAction(e->window.setScene(sceneSecond));
        goBackToScene1.setOnAction(e->window.setScene(sceneFirst));
        acceptEntry.setOnAction(e->{login = textLogin.getText(); password = textPassword.getText();
            System.out.println(login);
            System.out.println(password);});
        openNewWindow.setOnAction(e->{
            AnotherWindow newWindow = new AnotherWindow();
            newWindow.otherWindow("Other Window");
            System.out.println(dao.getX());
//            temp = newWindow.getTextBack();
        });
        System.out.println();
        window.show();

    }
}
