package javaFXexercises;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.Base64;

public class Ex1 extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        Button button1 = new Button("Click To Encode");
        Text text1 = new Text("Enter text to be encoded:");
        Text text2 = new Text("Encoded text: ");
        Label label1 = new Label("");

        TextField textField1 = new TextField("Enter text to be encoded");
        TextField textField2 = new TextField("Encoded text");

        VBox vBox = new VBox();
        vBox.getChildren().addAll(text1, textField1, button1, text2, textField2);

        primaryStage.setTitle("Encoding Base64");
        primaryStage.setScene(new Scene(vBox, 300, 120));
        primaryStage.show();

        button1.setOnAction(e -> {

            String value = textField1.getText();
            String encoded = Base64.getEncoder().encodeToString(value.getBytes());

            ClipboardContent clipboardContent = new ClipboardContent();
            clipboardContent.putString(encoded);
            Clipboard clipboard = Clipboard.getSystemClipboard();
            clipboard.setContent(clipboardContent);

            textField2.setText(encoded);
        });
    }
}
