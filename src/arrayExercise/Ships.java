package arrayExercise;

public class Ships {

    private int width;
    private int height;
    private  int numberOfShips;

    public Ships(int width, int height, int numberOfShips) {
        this.width = width;
        this.height = height;
        this.numberOfShips = numberOfShips;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    protected void drawMap() {
        int[][] map = new int[getWidth()][getHeight()];
        char w = 49, h = 65;
        boolean row = true;
        double randomNumber;
        int countShips=0;
        //double chance = (((double)numberOfShips)/(width*width))+(double)numberOfShips*0.8;

        for (int j = 0; j <= map.length; j++) {
            if (row == false) {
                System.out.println();
                System.out.print(w++ + "  ");
            }

            for (int i = 0; i < map.length; i++) {

                if (row == true) {
                    if (i == 0) {
                        System.out.print("  ");
                    }
                    System.out.print("  " + h++);
                    if (i == getHeight() - 1) {
                        row = false;
                    }
                } else {
                    randomNumber = Math.random();
                    if ((randomNumber>0.75)&&(countShips<numberOfShips)) {
                        System.out.print(" O ");
                        countShips++;}
                    else {
                        System.out.print(" . ");
                        }
                }
            }
        }
    }
}
