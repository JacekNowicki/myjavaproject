package arrayExercise;

public class Zad2 {

    public int getInts(int[][] tablice) {
        int suma = 0;
        for (int i = 0; i < tablice.length; i++) {
            for (int j = 0; j < tablice[i].length; j++) {
                suma = suma + tablice[i][j];
            }
        }
        return suma;
    }
}