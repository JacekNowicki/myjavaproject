package arrayExercise.apiShopExcersise;

public class Items {
    private String itemName;
    private double itemPrice;
    private int itemQuantity;

    protected Items(String itemName, double itemPrice, int itemQuantity) {
        this.itemName = itemName;
        this.itemPrice = itemPrice;
        this.itemQuantity = itemQuantity;
    }

    @Override
    public String toString() {
        return "Items{" +
                "itemName='" + itemName + '\'' +
                ", itemPrice=" + itemPrice +
                ", itemQuantity=" + itemQuantity +
                '}';
    }

    protected String getItemName() {
        return itemName;
    }

    protected double getItemPrice() {
        return itemPrice;
    }

    protected int getItemQuantity() {
        return itemQuantity;
    }
}
