package arrayExercise.apiShopExcersise;

import java.util.Scanner;

public class ShopTest {

    static void menu(){
        System.out.println("Co chcesz kupic? ");
        System.out.println("1. Zeszyt");
        System.out.println("2. Dlugopis");
        System.out.println("3. Olowek");
        System.out.println("4. Gumka");
        System.out.println("5. Flamaster");
        System.out.println("6. Pioro");
        System.out.println("q. *** KONIEC ZAKUPOW ***");
        System.out.println("Co wybierasz?");
    }
    public static void main(String[] args) {

        Scanner text = new Scanner(System.in);
        Scanner text1 = new Scanner(System.in);
        char entry;
        int quantity;
        double total=0.0;
        Cart itemToCart = new Cart();
        Items[] itemInCard = new Items[6];
        menu();

        do {
            entry = text.nextLine().charAt(0);
            switch (entry) {
                case '1':
                    System.out.println("wybrales zeszyt");
                    System.out.println("Ile chcesz zeszytow?");
                    quantity = text1.nextInt();
                    itemInCard[0] = itemToCart.addItem("Zeszyt", 18.99, quantity);
                    System.out.println("Czy cos jeszcze?");
                    menu();

                    break;
                case '2':
                    System.out.println("wybrales dlugopis");
                    System.out.println("Ile chcesz dlugopisow?");
                    quantity = text1.nextInt();
                    itemInCard[1] = itemToCart.addItem("Dlugopis", 25.99, quantity);
                    System.out.println("Czy cos jeszcze?");
                    menu();

                    break;
                case '3':
                    System.out.println("wybrales olowek");
                    System.out.println("Ile chcesz olowkow?");
                    quantity = text1.nextInt();
                    itemInCard[2] = itemToCart.addItem("Olowek", 8.99, quantity);
                    System.out.println("Czy cos jeszcze?");
                    menu();

                    break;
                case '4':
                    System.out.println("wybrales gumke");
                    System.out.println("Ile chcesz gumek?");
                    quantity = text1.nextInt();
                    itemInCard[3] = itemToCart.addItem("Gumka", 1.99, quantity);
                    System.out.println("Czy cos jeszcze?");
                    menu();

                    break;
                case '5':
                    System.out.println("wybrales flamaster");
                    System.out.println("Ile chcesz flamastrow?");
                    quantity = text1.nextInt();
                    itemInCard[4] = itemToCart.addItem("Flamaster", 7.99, quantity);
                    System.out.println("Czy cos jeszcze?");
                    menu();

                    break;
                case '6':
                    System.out.println("wybrales pioro");
                    System.out.println("Ile chcesz pior?");
                    quantity = text1.nextInt();
                    itemInCard[5] = itemToCart.addItem("Pioro", 39.99, quantity);
                    System.out.println("Czy cos jeszcze?");
                    menu();

                    break;

                case ('q'): {
                    break;
                }
                case ('Q'): {
                    break;
                }
                default:
                    System.out.println("WRONG ENTRY");
                    System.out.println("Czy cos jeszcze?");

            }
        }
        while ((entry != 'q') && (entry != 'Q'));
        System.out.println("Dziekujemy!");

        for (Items element:itemInCard){
            if (element!=null){
               System.out.println(element);
               total=total+ element.getItemPrice()*element.getItemQuantity();
            }
        }
        System.out.printf("Suma zakopow: %.02f",total);
    }

}
