package arrayExercise;

import javax.swing.*;

public class ShipsTest {

    public static void main(String[] args) {
        int size, numberOfShips;
        String entry = JOptionPane.showInputDialog("Wpisz wielkosc siatki:");
        size=Integer.valueOf(entry);

        entry = JOptionPane.showInputDialog("Wpisz max ilosc statkow:");
        numberOfShips=Integer.valueOf(entry);

        if (numberOfShips>size*size) {numberOfShips=size*size;}


        Ships mapa = new Ships(size,size,numberOfShips);
        mapa.drawMap();

    }

}
