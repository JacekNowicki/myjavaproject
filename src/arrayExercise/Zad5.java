package arrayExercise;

import static java.lang.Math.round;

public class Zad5 {

    public void ListAndAv(double... tab_5) {
        System.out.println(tab_5[0]);
        System.out.println(tab_5[round(tab_5.length / 2)]);
        System.out.println(tab_5.length);
        double sum = 0.0;
        for (int i = 0; i < tab_5.length; i++) {
            sum = tab_5[i] + sum;
        }
        System.out.println(sum / tab_5.length);
    }
}
