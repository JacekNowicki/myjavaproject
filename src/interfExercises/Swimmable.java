package interfExercises;

public interface Swimmable {
    boolean swim();

    void setSwimming(boolean b);
}
