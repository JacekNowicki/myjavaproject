package interfExercises.species;

import interfExercises.Animal;
import interfExercises.Swimmable;

public class Fish implements Animal, Swimmable {

    private int age;
    private boolean isSwimming;

    public Fish(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Fish{" +
                "age=" + age + " " + getName() + " " + " " + speak() +", is alive? - " +isAlive(getAge())+
                ", isSwimmin - "+swim()+
                '}';
    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public String getName() {
        return "Fishy";
    }

    @Override
    public String speak() {
        return "plum";
    }


    @Override
    public boolean swim(){
        return this.isSwimming;
    }

    public void setSwimming(boolean swimming) {
        isSwimming = swimming;
    }
}
