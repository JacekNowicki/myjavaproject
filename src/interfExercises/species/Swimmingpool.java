package interfExercises.species;

import interfExercises.Swimmable;

public class Swimmingpool {

    private Swimmable[] swimanim;

    public Swimmingpool(Swimmable... swimmingAnimals) {

        this.swimanim = swimmingAnimals;
    }

    public void willSwim() {
        for (Swimmable animal : swimanim) {
            animal.setSwimming(true);
        }

    }
}

