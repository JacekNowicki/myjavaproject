package interfExercises.species;

import interfExercises.Animal;
import interfExercises.Swimmable;

public class Reptiles implements Animal, Swimmable {

    private int age;
    private boolean isSwimming;

    public Reptiles(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Reptiles{" +
                "age=" + age + " " + getName() + " " + " " + speak() +", is alive? - " +isAlive(getAge())+
                ", isSwimmin - "+swim()+
                '}';
    }


    @Override
    public int getAge() {
        return age;
    }

    @Override
    public String getName() {
        return "Reptile";
    }

    @Override
    public String speak() {
        return "hrrrr";
    }

    @Override
    public boolean isAlive(int age) {
        return (age<(MAX_AGE-1));
    }

    public void setSwimming(boolean swimming) {
        isSwimming = swimming;
    }

    @Override
    public boolean swim(){
        return this.isSwimming;


    }
}
