package interfExercises.species;

import interfExercises.Animal;
import interfExercises.Flayable;

public class Insects implements Flayable, Animal {

    private int age;

    @Override
    public String toString() {
        return "Insects{" +
                "age=" + age +" "+ getName() +" " + " "+ speak()+", is alive? - " +isAlive(getAge())+
                '}';
    }

    public Insects(int age) {
        this.age = age;
    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public String getName() {
        return "Flies";
    }

    @Override
    public String speak() {
        return "Bzzzz";
    }

    @Override
    public boolean fly() {
        return true;
    }
}
