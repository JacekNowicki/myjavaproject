package interfExercises.species;

public class Tree implements Plant {

    private int age;

    public Tree(int age) {
        this.age = age;
    }

    @Override
    public int getAge() {
        return age;
    }


    @Override
    public String toString() {
        return "Tree{" +
                "age=" + getAge() + " is alive? " + isAlive(age)+
                '}';
    }
}
