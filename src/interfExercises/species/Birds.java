package interfExercises.species;

import interfExercises.Animal;
import interfExercises.Flayable;

public class Birds implements Animal, Flayable {

    private int age;

    public Birds(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Birds{" +
                "age=" + age + " " + getName() + " " + " " + speak() +", is alive? - " +isAlive(getAge())+
                '}';
    }


    @Override
    public int getAge() {
        return age;
    }

    @Override
    public boolean isAlive(int age) {
        return false;
    }

    @Override
    public String getName() {
        return "Birdy";
    }

    @Override
    public String speak() {
        return "kwak";
    }


    @Override
    public boolean fly() {
        return true;
    }
}
