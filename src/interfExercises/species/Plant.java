package interfExercises.species;

import interfExercises.Being;

public interface Plant extends Being {

    @Override
    default boolean isAlive(int age){

        return (age<1000);
    }


}
