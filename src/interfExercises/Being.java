package interfExercises;

public interface Being {

    int MAX_AGE = 100;
    int getAge();

     default boolean isAlive(int age){
        return (age<MAX_AGE);
    }

}
