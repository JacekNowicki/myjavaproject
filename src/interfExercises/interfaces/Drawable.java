package interfExercises.interfaces;

public interface Drawable {
    void draw();
}