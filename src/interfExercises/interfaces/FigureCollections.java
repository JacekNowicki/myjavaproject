package interfExercises.interfaces;

import java.util.ArrayList;
import java.util.List;

public class FigureCollections {

    public static void main(String[] args) {


        Rectangle rectangle = new Rectangle(10,20);
        Circle circle = new Circle(10);
        Triangle triangle = new Triangle(10,7);

        List<Figure> figure = new ArrayList<>();

        figure.add(rectangle);
        figure.add(circle);
        figure.add(triangle);

        for(Figure element: figure){
            System.out.println(element.getType()+ " Area: "+ element.getArea());
            System.out.println(element.getType()+" Peremeter: "+element.getPerimeter());
        }

    }

}
