package interfExercises;

import interfExercises.species.*;

public class InerfTest {

    public static void main(String[] args) {
        Insects fly = new Insects(10);
        Birds bird = new Birds(25);
        Fish fish = new Fish(5);
        Reptiles reptile = new Reptiles(120);

        System.out.println(fly);
        System.out.println(bird);
        System.out.println(fish);
        System.out.println(reptile);


        Tree oak = new Tree(350);
        System.out.println(oak);


        Swimmingpool pool = new Swimmingpool(fish, reptile);
        pool.willSwim();
        System.out.println(fish);
        System.out.println(reptile);

    }
}
