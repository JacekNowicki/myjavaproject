package abstrExercises.food.differentFood;

import abstrExercises.food.foodNature.Vegetable;

public class Carrot extends Vegetable {
    public Carrot(String name) {
        super(name);
    }

    @Override
    public String getTaste() {
        return "smak marchewki";
    }
}
