package abstrExercises.food.differentFood;

import abstrExercises.food.foodNature.Meat;

public class Ham extends Meat {
    public Ham(String name) {
        super(name);
    }

    @Override
    public String getTaste() {
        return "smak szynki";
    }
}
