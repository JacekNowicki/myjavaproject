package abstrExercises.food.differentFood;

import abstrExercises.food.foodNature.Meat;

public class Chicken extends Meat {
    public Chicken(String name) {
        super(name);
    }

    @Override
    public String getTaste() {
        return "smak kurczaka";
    }

}
