package abstrExercises.food.differentFood;

import abstrExercises.food.foodNature.Vegetable;

public class Salad extends Vegetable {
    public Salad(String name) {
        super(name);
    }

    @Override
    public String getTaste() {
        return "smak salaty";
    }
}
