package abstrExercises.food;

// import locg4j;

import abstrExercises.food.differentFood.Carrot;
import abstrExercises.food.differentFood.Chicken;
import abstrExercises.food.differentFood.Ham;
import abstrExercises.food.differentFood.Salad;

public class FoodTest {

    public static void main(String[] args) {

        Carrot carrot = new Carrot("Marchewka");
        Chicken chicken = new Chicken("Kurczak");
        Ham ham = new Ham("Szynka");
        Salad salad = new Salad("Salata");

//        System.out.println(carrot.decribe());
//        System.out.println(chicken.decribe());
//        System.out.println(ham.decribe());
//        System.out.println(salad.decribe());

        Recipe recipe1 = new Recipe("Danie z wszystkich Skladnikow", carrot, chicken, ham, salad);
        System.out.println(recipe1.getDiscription());
        System.out.println("...........");
        System.out.println(recipe1.getIngrNames());


        Recipe recipe2 = new Recipe("Danie z wybranych skladnikow", 3);
        recipe2.addIngr(chicken);
        recipe2.addIngr(carrot);
        recipe2.addIngr(ham);

        System.out.println(recipe2.getDiscription());
        System.out.println("...........");
        System.out.println(recipe2.getIngrNames());
    }
}
