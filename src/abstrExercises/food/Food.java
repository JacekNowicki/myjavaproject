package abstrExercises.food;

import abstrExercises.food.differentFood.FoodEnum;

public abstract class Food {

    String name;

    protected abstract String getTaste();

    protected abstract FoodEnum getType();

    protected String decribe() {
        return getName() + " " + getType() + " " + getTaste();
    }

    protected String decribe2() {
        return getName() + " --> " + getTaste() + " - " + getType();
    }

    public Food(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
