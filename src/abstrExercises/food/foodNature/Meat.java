package abstrExercises.food.foodNature;

import abstrExercises.food.Food;
import abstrExercises.food.differentFood.FoodEnum;

public abstract class Meat extends Food {

    public Meat(String name) {
        super(name);
    }

    @Override
    protected abstract String getTaste();

    @Override
    protected FoodEnum getType() {
        return FoodEnum.MEAT;
    }
}
