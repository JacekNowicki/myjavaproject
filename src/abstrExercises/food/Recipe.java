package abstrExercises.food;


public class Recipe {

    private String recipeName;
    private Food[] ingrTab;
    private int nrOfIngr = 0;
    private static int counter = 0;

    protected Recipe(String recipeName, Food... ingridiens) {
        this.recipeName = recipeName;
        this.ingrTab = ingridiens;
        this.nrOfIngr = this.ingrTab.length;
    }

    public Recipe(String recipeName, int nrOfIngr) {
        this.recipeName = recipeName;
        this.nrOfIngr = nrOfIngr;
        this.ingrTab = new Food[nrOfIngr];
    }

    public Food[] addIngr(Food ingridien) {
        this.ingrTab[counter] = ingridien;
        counter++;
        return ingrTab;
    }

    public String getIngrNames() {
        String ingrNames = "";
        for (Food element : ingrTab) {
            ingrNames = element.getName() + "\n" + ingrNames;
        }
        return recipeName + "\n" + ingrNames;
    }

    public String getDiscription() {
        String discrSummary = "";
        for (Food element : ingrTab) {
            discrSummary = element.decribe2() + "\n" + discrSummary;
        }
        return discrSummary;
    }

}
