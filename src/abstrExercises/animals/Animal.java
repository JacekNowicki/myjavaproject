package abstrExercises.animals;

public abstract class Animal {

    private String typeOfVOice;

    public abstract String getVoice();

 //   public abstract String getType();


    public String speak() {
        return " wydaje nastepujacy odglos: " + getVoice();
    }

}
