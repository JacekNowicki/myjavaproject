package generics;

public enum Gender {
    MALE,
    FEMALE,
    OTHER;
}
