package generics.exercises;

import java.util.*;

public class Tasks {

    public static void main(String[] args) {
        //#1
        //Set<Double> doubles = setOfDoubles();

        //#2
        //Map<Integer, String> genericMap = genericMap();

        //#3
        //List<Number> numbers = doublesToNumbers(doubles);
        //System.out.println("numbers = " + numbers);

        //#4
        //NumbersBox numbersBox = numbersToNumbersBox(numbers);

        //#5
        //NumbersBox<Double> doubleNumbersBox = boxOfDoubles(doubles);
    }

    /**
     * 1. Stwórz kolekcję typu Set, zawierającą obiekty klasy Double. Dodaj do niej kilka liczb.
     * Następnie pobierz i wyświetl w konsoli wszystkie elementy kolekcji.
     * Metoda powinna zwrócić stworzoną kolekcję.
     */
    private static Set<Double> setOfDoubles() {

        Set<Double> kolekcja = new HashSet<>();
        kolekcja.add(10.4);
        kolekcja.add(5.6);

        for (Double element:kolekcja){
            System.out.println(element);
        }
        return kolekcja;
    }

    /**
     * 2. Stwórz mapę zawierającą obiekty typu Integer jako klucze i String jako wartości. Dodaj kilka obiektów do mapy.
     * Pobierz i wyświetl w konsoli pojedyncze elementy.
     * Następnie pobierz i wyświetl w konsoli wszystkie elementy mapy.
     * Metoda powinna zwrócić stworzoną mapę.
     */
    private static Map<Integer, String> genericMap() {

        Map<Integer, String> mapa = new HashMap<>();

        mapa.put(1,"jeden");
        mapa.put(2,"dwa");

        System.out.println(mapa.get(1));
        System.out.println(mapa.get(2));

    return mapa;
    }

    /**
     * 3. Skopiuj podany zbiór liczby Double do listy obiektów klasy Number.
     * Nie kopiuj obiektu jeżeli jest równy null.
     * Zwróć nową listę jako wynik metody.
     */
    private static List<Number> doublesToNumbers(Set<Double> doubles) {

        List<Number> numberList = new ArrayList<>();

        for (Double element: doubles){
            if (element!=null) {numberList.add(element);}
        }

        return numberList;
    }

    /**
     * 4. Stwórz klasę NumbersBox (zwykłą lub wewnętrzną) która będzie przechowywać listę obiektów klasy Number.
     * Dodaje metody które:
     * - sprawdzą czy lista jest pusta
     * - pobierze pierwszy element listy
     * - pobierze pierwszy element listy jako int
     * - pobierze ostatni element listy
     * - pobierze ostatni element listy jako int
     * Zwróć obiekt klasy NumbersBox jako wynik tej metody.
     */
    private static void  numbersToNumbersBox(List<Number> numbers) {
    }

    /**
     * 5. Zmień klasę NumbersBox - tak żeby przyjmowała różne typy liczbowe - np. Double
     * Zwróć obiekt klasy NumbersBox jako wynik tej metody.
     */
    private static void boxOfDoubles(Set<Double> doubles) {
    }
}