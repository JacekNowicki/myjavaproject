package compAndPolim;

public class Hammer extends Tool{

    private double hammerWeight;

    public Hammer(String model, double price, double hammerWeight) {
        super(model, price);
        this.hammerWeight = hammerWeight;
    }

    public double getHammerWeight() {
        return hammerWeight;
    }

    public void setHammerWeight(double hammerWeight) {
        this.hammerWeight = hammerWeight;
    }

    @Override
    public String toString() {
        return super.toString()+" weight: "+hammerWeight;
    }
}
