package compAndPolim;

import javax.swing.*;

public class ShopCart {
    private String entry = "";
    private String model = "";
    private double price;

    public static void main(String[] args) {
        String entry = JOptionPane.showInputDialog("Ile przedmiotow chcesz kupic:");
        int x = Integer.valueOf(entry);
        Tool[] tools = new Tool[x];
        ShopCart shop = new ShopCart();

        for (int i = 0; i < tools.length; i++) {
            tools[i] = shop.addItem(i);
        }

        shop.list(tools);
        System.out.println("Cena zakupow: " + shop.sumShopCart(tools));
    }

    protected Tool addItem(int i) {
        entry = JOptionPane.showInputDialog("Przedmiot nr " + (i + 1) + " ; model: ");
        model = entry;
        entry = JOptionPane.showInputDialog("Przedmiot nr " + (i + 1) + " ; cena: ");
        price = Double.valueOf(entry);

        Tool tool_new = new Tool(model, price);
        return tool_new;
    }

    protected void list(Tool[] cart) {
        System.out.println("Lista Zakupow: ");
        System.out.println();
        for (Tool element : cart) {
            System.out.println(element);
        }
        System.out.println();
    }

    protected double sumShopCart(Tool[] cart) {
        double sum = 0.0;
        for (Tool element : cart) {
            sum += element.getPrice();
        }
        return sum;
    }
}
