package compAndPolim;

public class ToolShop {

    public static void main(String[] args) {

        Hammer hammer1 = new Hammer("HAM_1.0",49.99,6);
        Hammer hammer2 = new Hammer("HAM_2.0",69.99,12);
        Hammer hammer3 = new Hammer("HAM_3.0",29.99,4);

        Saw saw1 = new Saw("SAW_1.0",12.99,14);
        Saw saw2 = new Saw("SAW_2.0",11.99,8);
        Saw saw3 = new Saw("SAW_3.0",22.99,18);

        System.out.println(hammer1);
        System.out.println(saw1);

    }

}
