package compAndPolim;

public class Saw extends Tool{

    private double sawLenght;

    public Saw(String model, double price, double sawLenght) {
        super(model, price);
        this.sawLenght = sawLenght;
    }

    public double getSawLenght() {
        return sawLenght;
    }

    public void setSawLenght(double sawLenght) {
        this.sawLenght = sawLenght;
    }

    @Override
    public String toString() {
        return super.toString()+" lenght: "+sawLenght;
    }
}
