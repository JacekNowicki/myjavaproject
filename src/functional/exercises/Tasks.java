package functional.exercises;

import java.util.DoubleSummaryStatistics;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Tasks {

    public static void main(String[] args) {
        //#1
     //   useLambdas();

        //#2
        useStreams();
    }


    /**
     * 1. Stwórz i użyj lambdę która:
     * - stworzy obiekt klasy Double (Supplier)
     * - pobierze obiekt klasy String i wyświetli na konsolę (Consumer)
     * - sprawdzi czy podany string ma długość > 10 (Predictable)
     * - przekształci dwie liczby w String (zsumuje je i zwróci reprezentację tekstową) (BiFunction)
     */
    private static void useLambdas() {

        Supplier<Double> dbl = () -> 11.5;
        System.out.println(dbl.get());

        Consumer<String> consumer = (str) -> System.out.println(str);
        consumer.accept("Hello");

        Predicate<String> predicate = (str) -> str.length() > 10;
        System.out.println(predicate.test("hello"));

        BiFunction<Integer, Integer, String> biFunction = (x, y) -> {
            int sum = x + y;
            return String.valueOf(sum);
        };
        System.out.println(biFunction.apply(10,20));
    }
//

//        Supplier<Double> doubleSupplier = () -> 10.6;
//        System.out.println("doubleSupplier = " +doubleSupplier.get());
//
//        Consumer<String> stringConsumer = (str) -> System.out.println("str = " + str);
//        stringConsumer.accept("Hello");
//
//        BiFunction<Integer,Integer,String> bifunction = (num1,num2) -> {
//            int result = num1 + num2;
//        return String.valueOf(result);
//        };
//        String result =bifunction.apply(1,2);
//        System.out.println("result = " + result);
//        }
//


        /**
         * 2. Stwórz i użyj strumień danych (Stream):
         * - stwórz stream liczb typu Double z kolekcji typu Set - podaj ich sumę i średnią arytmetyczną
         * - stwórz stream liczb całkowitych od 10 do 40, usuń parzyste i podaj sumę pozostałych
         * - stwórz stream obiektów typu String, zamień wszystkie litery na małe, zostaw tylko te które zaczynają się na literę 'a' lub 'z'
         *   i stwórz listę przetworzonych elementów
         * - stwórz stream obiektów Person i stwórz statystykę lat (suma, średnia, minimalny i maksymalny wiek) dla tego zbioru
         * - stwórz strumień który wypisze na ekran ścieżki wszystkich katalogów i podkatalogów znajdujących się w aktualnym katalogu (Path.get("."))
         */
        private static void useStreams () {

            Set<Double> doubles = new HashSet<>();
            doubles.add(3.6);
            doubles.add(4.5);
            doubles.add(7.8);
            DoubleSummaryStatistics doubleSummaryStatistics = doubles.stream()
                    .collect(Collectors.summarizingDouble(d ->d));
            System.out.println(doubleSummaryStatistics.getSum());
            System.out.println(doubleSummaryStatistics.getAverage());


            int sum = IntStream.range(10,40)
                    .filter(num -> num%2!=0)
                    .sum();
            System.out.println(sum);


            Stream<String> stringStream = Stream.of("Ala","ma","kota");
            List<String> lista = stringStream
                    .map(str -> str.toLowerCase())
                    .filter(str->str.startsWith("a")|| str.startsWith("k"))
                    .collect(Collectors.toList());
            System.out.println(lista);


            Stream<Person> personStream = Stream.of(new Person(15, "Ola"),
                    new Person(35, "Mateusz"), new Person(23, "Jarek"), new Person(30, "Marek"));



        }





}
