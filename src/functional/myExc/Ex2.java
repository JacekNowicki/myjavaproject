package functional.myExc;

import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;

public class Ex2 {

    public static void main(String[] args) {

        List<Integer> lista = new ArrayList<>();
        lista.add(2);
        lista.add(5);
        lista.add(8);

        IntSummaryStatistics intSummaryStatistics = lista.stream()
                .filter(x -> x % 2 == 0)
                .collect(Collectors.summarizingInt(x -> x));
        System.out.println(intSummaryStatistics.getSum());

        int sum = lista.stream()
                .mapToInt(x -> x)
                .filter(x -> x % 2 == 0)
                .sum();
        System.out.println(sum);


    }
}
