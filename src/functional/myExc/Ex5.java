package functional.myExc;

import functional.exercises.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Ex5 {

    public static void main(String[] args) {

        List<Person> persons = new ArrayList<>(Arrays.asList(new Person(20, "Ania"), new Person(31, "Łukasz"),
                new Person(24, "Feliks"), new Person(8, "Paweł")));


        String string = persons.stream()
                .map(Person::getName)
                .sorted()
                .map(x->x.toUpperCase())
                .limit(5)
                .collect(Collectors.joining(","));

        System.out.println(string);
    }

}
