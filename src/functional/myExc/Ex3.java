package functional.myExc;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Ex3 {

    public static void main(String[] args) {

        List<Integer> lista = new ArrayList<>();
        lista.add(2);
        lista.add(5);
        lista.add(8);


        List<Integer> newList = lista.stream()
                .map(x->x*2)
                .collect(Collectors.toList());

        System.out.println(newList);


    }

}
