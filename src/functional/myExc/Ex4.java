package functional.myExc;

import functional.exercises.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Ex4 {

    public static void main(String[] args) {

        List<Person> persons = new ArrayList<>(Arrays.asList(new Person(20, "Ania"), new Person(31, "Łukasz"),
                new Person(24, "Feliks"), new Person(8, "Paweł")));

        System.out.println(persons);

        System.out.println("wprowadz imie: ");
        Scanner entry = new Scanner(System.in);
        String name = entry.nextLine();


        findByName(persons, name);

        if (findByName(persons, name).isEmpty()) {
            System.out.println("No such name");
        } else {
            System.out.println("Found: "+ findByName(persons,name));
        }
    }
    private static List<Person> findByName(List<Person> list, String name) {

        List<Person> result = list.stream()
              //  .filter(x -> name.equals(x.getName()))
                .filter(x -> (x.getName()).equals(name))
                //      .forEach(System.out::println);
                .collect(Collectors.toList());
                //  System.out.println(result);
        return result;
    }

}
