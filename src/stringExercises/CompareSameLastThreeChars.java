package stringExercises;

import java.util.Scanner;

public class CompareSameLastThreeChars {

    public static void main(String[] args) {

        CompareSameLastThreeChars testObjectCompare = new CompareSameLastThreeChars();
        Scanner entryText = new Scanner(System.in);

        System.out.println("Wpisz tekst nr 1 do porownania: ");
        String text1 = entryText.nextLine();
        System.out.println("Wpisz tekst nr 2 do porownania: ");
        String text2 = entryText.nextLine();

        System.out.println(testObjectCompare.CompareTwoStrings(text1, text2));
    }

    protected boolean CompareTwoStrings(String text1, String text2) {
        String t1, t2;
        t1 = text1.substring(text1.length() - 3);
        System.out.println("trimmed1: " + t1);
        t2 = text2.substring(text2.length() - 3);
        System.out.println("trimmed2: " + t2);

        if (t1.equalsIgnoreCase(t2)) {
            return true;
        } else return false;
    }
}
