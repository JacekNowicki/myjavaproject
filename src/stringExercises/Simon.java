package stringExercises;

public class Simon {

    public static void main(String[] args) {

        Simon simon = new Simon();

        System.out.println(simon.back(("HEY")));

    }

    protected String back(String text1) {
        String text2 = new StringBuilder("Simon says: ").append(text1).toString();
        return text2;

    }

}
