package stringExercises;

import java.util.Scanner;

public class CheckThreeTimesNie {
    private int counter;

    public static void main(String[] args) {
        Scanner entryText = new Scanner(System.in);
        System.out.println("Wpisz tekst: ");
        String enteredText = entryText.nextLine();
        CheckThreeTimesNie text = new CheckThreeTimesNie();
        System.out.println("Slowo \"nie\" wystapilo " + text.checkNIE(enteredText) + " razy");
    }

    protected int checkNIE(String text) {

        for (int i = 0; i < (text.length() - 2); i++) {
            if ((text.substring(i, i + 3)).equalsIgnoreCase("nie")) {
                counter++;
            }
        }
        return counter;
    }

}
