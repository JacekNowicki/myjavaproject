package stringExercises;

import java.util.Scanner;

public class CompareSameFirstChar {

    public static void main(String[] args) {

        CompareSameFirstChar testObjectCompare = new CompareSameFirstChar();
        Scanner entryText1 = new Scanner(System.in);
        Scanner entryText2 = new Scanner(System.in);

        System.out.println("Wpisz tekst nr 1 do porownania: ");
        String text1 = entryText1.nextLine();
        System.out.println("Wpisz tekst nr 2 do porownania: ");
        String text2 = entryText2.nextLine();

        boolean isEven = testObjectCompare.CompareTwoStrings(text1, text2);
        System.out.println(isEven);
    }

    protected boolean CompareTwoStrings(String text1, String text2) {

        if (text1.charAt(0) == text2.charAt(0)) {
            return true;
        } else return false;
    }
}
