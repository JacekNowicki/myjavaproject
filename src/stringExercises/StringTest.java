package stringExercises;

import enumExercise.Color;

public class StringTest {

    public static void main(String[] args) {

        Color col1 = Color.BLACK;
        System.out.println(col1);

        String text="  Monday  ";

        System.out.println(text);
        System.out.println(text.trim());        //trim
        text = text.trim();
        System.out.println(text.toUpperCase()); //uppersace
        System.out.println(text.toLowerCase()); //lowercase

        System.out.println(text.length());
        System.out.println(text.charAt(3));
        System.out.println(text.indexOf("o"));
        System.out.println(text.substring(1,4));
        System.out.println(text.startsWith("M"));
        System.out.println(text.concat(" Tuesday"));
        System.out.println(text.contains("on"));
        System.out.println(text.replace("Mon","Tues"));

        text="MoNdAy";
        if (text.equalsIgnoreCase("monday")) {
            System.out.println("true");}
    }

}

