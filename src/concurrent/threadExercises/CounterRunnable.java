package concurrent.threadExercises;

public class CounterRunnable implements Runnable{

    private CounterEx4 counterEx4;

    public CounterRunnable(CounterEx4 counterEx4) {
        this.counterEx4=counterEx4;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
        System.out.println();

        counterEx4.increaseCounter();
        System.out.println(counterEx4.getCounter());

    }
}
