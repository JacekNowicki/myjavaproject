package concurrent.threadExercises;

public class CustThread extends Thread {

    CustThread(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
    }
}