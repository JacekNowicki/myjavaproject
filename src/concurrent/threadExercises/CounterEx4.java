package concurrent.threadExercises;

public class CounterEx4 {

    private static int counter;

    public void increaseCounter(){
        counter++;
    }

    public int getCounter() {
        return counter;
    }
}
