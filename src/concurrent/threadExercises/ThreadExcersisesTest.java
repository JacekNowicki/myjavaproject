package concurrent.threadExercises;

public class ThreadExcersisesTest {

    public static void main(String[] args) {

//        Ex3 custRunnable = new Ex3(23456);
//        Thread thread = new Thread(custRunnable);
//        thread.start();
//
//        System.out.println("ExecutorService");
//
//        ExecutorService executorService = Executors.newFixedThreadPool(4);
//        executorService.execute(new Ex3(2345));
//        executorService.shutdown();


        CounterRunnable counterRunnable1 = new CounterRunnable(new CounterEx4());
        CounterRunnable counterRunnable2 = new CounterRunnable(new CounterEx4());
        CounterRunnable counterRunnable3 = new CounterRunnable(new CounterEx4());
        CounterRunnable counterRunnable4 = new CounterRunnable(new CounterEx4());
        CounterRunnable counterRunnable5 = new CounterRunnable(new CounterEx4());
        Thread threadCounter1 = new Thread(counterRunnable1);
        Thread threadCounter2 = new Thread(counterRunnable2);
        Thread threadCounter3 = new Thread(counterRunnable3);
        Thread threadCounter4 = new Thread(counterRunnable4);
        Thread threadCounter5 = new Thread(counterRunnable5);
        threadCounter1.start();
        threadCounter2.start();
        threadCounter3.start();
        threadCounter4.start();
        threadCounter5.start();

    }
}
