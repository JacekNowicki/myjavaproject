package concurrent.threadExercises;

public class Ex2 {

    public static void main(String[] args) {

        System.out.println("running customthread");
        CustThread thread = new CustThread("thread by me");
        thread.start();


        System.out.println("running custom runnable");
        CustRunnable custRunnable = new CustRunnable();
        Thread thread1 = new Thread(custRunnable);
        thread1.start();



    }

}
