package concurrent.threadExercises;

import java.util.ArrayList;
import java.util.List;

public class Ex3 implements Runnable{
    private int number;

    public Ex3(int number) {this.number=number;}

    @Override
    public void run() {

        System.out.println(Thread.currentThread().getName());

        System.out.println();
        System.out.println("Method starting!");
        long start = System.currentTimeMillis();

        List<Integer> list = new ArrayList<>();
        for (int i=2; i<number;i++){
            if (number%i==0) {list.add(i);}
        }
        if (list.isEmpty()) {
            System.out.println(number + " has no natural dividers");
        }
        else{
            System.out.print("Natural deviders of "+number+" are: ");
            for (Integer element:list) {
                System.out.print(element+"; ");
            }
        }
        System.out.println();
        System.out.println("Total time: "+(System.currentTimeMillis()-start));

    }
}
