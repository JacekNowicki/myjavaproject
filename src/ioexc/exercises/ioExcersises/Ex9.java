package ioexc.exercises.ioExcersises;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Ex9 {

    public static void main(String[] args) {

        Path folder = Paths.get("c://JavaTest");
        Path sub;

        for (int i = 0; i <= 900; i = i + 100) {

            try {
                sub = folder.resolve("./" + String.valueOf(i));
                System.out.println(sub.toAbsolutePath());
                Files.createDirectories(sub);

                for (int j = 0; j <= 90; j = j + 10) {
                    Path file = sub.resolve("./" + (i + j + 1) + "-" + (i + j + 10) + ".txt");
                    if (Files.notExists(file)) Files.createFile(file);

                    BufferedWriter writer = Files.newBufferedWriter(file);
                    int sum = 0;
                    for (int a = (i + j + 1); a <= (i + j + 10); a++) {
                        sum = sum + a;

                    }
                    System.out.println(sum);
                    writer.write(String.valueOf(sum));
                    writer.close();

                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
