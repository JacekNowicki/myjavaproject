package ioexc.exercises.ioExcersises;

import java.io.File;
import java.io.IOException;

public class Ex1 {

    public static void main(String[] args) {

        File folder1 = new File("c://javafileexcersises", "test1");
        File folder2 = new File("c://javafileexcersises", "test2");
        File plik1 = new File("c://javafileexcersises", "test1.txt");
        File plik2 = new File("c://javafileexcersises", "test2.txt");

        folder1.mkdir();
        folder2.mkdir();
        try {
            plik1.createNewFile();
            plik2.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        folder1 = new File("c://javafileexcersises/test1", "test3");
        folder2 = new File("c://javafileexcersises/test2", "test4");
        plik1 = new File("c://javafileexcersises/test1/test3", "test3.txt");
        plik2 = new File("c://javafileexcersises/test2/test4", "test4.txt");

        folder1.mkdir();
        folder2.mkdir();
        try {
            plik1.createNewFile();
            plik2.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        plik2.renameTo(plik1);

        File[] files = folder1.listFiles();
      //  listFiles(files);

    }
}