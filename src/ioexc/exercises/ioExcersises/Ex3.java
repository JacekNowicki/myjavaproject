package ioexc.exercises.ioExcersises;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Ex3 {

    public static void main(String[] args) {

        Path folder = Paths.get("c://JavaEx3");
        Path folder2 = folder.resolve("./folder2");

        Path file = folder.resolve("./name.txt");

        try {
          //  Files.createDirectories(folder);
            Files.createDirectories(folder2);
            if (Files.notExists(file)) Files.createFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        try (BufferedWriter writer = Files.newBufferedWriter(file)){
//        writer.write("Jacek Nowicki");
//        } catch (IOException e) {
//            e.printStackTrace();
//    }
        try (BufferedWriter writer = Files.newBufferedWriter(file)){
            writer.write("Agata Nowicki");
            writer.newLine();
            writer.write("Jacek Nowicki");

        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedReader reader = Files.newBufferedReader(file)){
            System.out.println(reader.readLine());
            System.out.println(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
