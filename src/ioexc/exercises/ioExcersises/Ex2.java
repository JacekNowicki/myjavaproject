package ioexc.exercises.ioExcersises;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Ex2 {

    public static void main(String[] args) {

        Path folder = Paths.get("c://JavaTestIO");
        try {
            Files.createDirectories(folder);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Path folderIn = folder.resolve("./Folder");
        Path folder1File = folder.resolve("./folder1/tekst1.txt");
        Path folder2File = folder.resolve("./folder2/tekst2.txt");

        try {
            Files.createDirectories(folder1File.getParent());
            Files.createDirectories(folder2File.getParent());
            Files.createDirectories(folderIn);
            if (!Files.exists(folder1File)) Files.createFile(folder1File);
            if (!Files.exists(folder2File))Files.createFile(folder2File);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            System.out.println(folder1File.toRealPath());
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            Files.write(folder1File, "Hello 1".getBytes());
            Files.write(folder2File, "Hello 2".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Path folderNewFile1 = folder.resolve(folder1File.getFileName());

        try {
            Files.copy(folder1File,folderNewFile1, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }


        Path folderNewFile2 = folder.resolve(folder2File.getFileName());

        try {
            Files.move(folder2File,folderNewFile2, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (Stream<Path> list = Files.walk(folder)) {
            list.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Stream<Path> listToDelete = Files.list(folder);
            List<Path> lista = listToDelete.collect(Collectors.toList());
            for (Path element: lista){
                if (Files.isRegularFile(element)) {
                    Files.delete(element);
                }
                else{
                    if (Files.isDirectory(element)){
                        Files.delete(element);
                    }
                }
            }



        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
