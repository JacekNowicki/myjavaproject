package ioexc.exercises.ioExcersises;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class Ex5 {

    public static void main(String[] args) {

        Path folder = Paths.get("c://JavaEx3");
        Path newFile = folder.resolve("./name.txt");
        Path originalFile = folder.resolve("./BlizzardError.exe");

        try {
            System.out.println(newFile.toRealPath());
            System.out.println(originalFile.toRealPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Files.copy(originalFile,newFile, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
