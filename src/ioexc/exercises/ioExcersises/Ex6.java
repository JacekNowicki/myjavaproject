package ioexc.exercises.ioExcersises;

import ioexc.exercises.Student;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Ex6 {

    public static void main(String[] args) {

        Student student1 = new Student(1,"Jan","finanse");
        Student student2 = new Student(2,"Adam","operacje");

        System.out.println(student1);
        System.out.println(student2);

        Path folder = Paths.get("c://Java_student");
        Path file = folder.resolve("./students.txt");

        try {
            Files.createDirectories(file.getParent());
            if (Files.notExists(file)) Files.createFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedWriter writer = Files.newBufferedWriter(file)) {
            writer.write(student1.toString());
            writer.newLine();
            writer.write(student2.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
