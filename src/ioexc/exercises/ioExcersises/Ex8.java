package ioexc.exercises.ioExcersises;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class Ex8 {

    public static void main(String[] args) {

        Path file = Paths.get("c://JavaTekst/tekst2.txt");

        System.out.println(file.toAbsolutePath());

        try {
            BufferedReader reader = Files.newBufferedReader(file);
            String string = reader.readLine();
            reader.close();

            string= string.toLowerCase();
            System.out.println(string);
            string= string.replace(".","");
            string= string.replace("!","");
            string =string.replace("?","");
            string =string.replace(",","");
            string=string.replace("...","");
            System.out.println(string);

            String a = " ";
            String[] words = string.split(a);
//            System.out.println();

            Arrays.sort(words);

            for (int i=0; i<words.length;i++){

                if (words[i]!=null) {
                    int counter = 1;
                    for (int j = (i + 1); j < words.length; j++) {

                        if (words[i].equals(words[j])) {
                            counter++;
                            words[j] = null;
                        }
                    }
                    System.out.printf(words[i] + " --> " + counter +"/"+words.length+"  --> %.2f"+"%%",(((double)counter/words.length)*100));
                    System.out.println();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
