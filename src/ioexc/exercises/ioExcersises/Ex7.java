package ioexc.exercises.ioExcersises;

import ioexc.exercises.Student;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Ex7 {

    public static void main(String[] args) {


        Path folder = Paths.get("c://Java_student");
        Path file = folder.resolve("./students.txt");
        ArrayList<Student> student = new ArrayList<>();

        try (BufferedReader reader = Files.newBufferedReader(file)) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] record = line.split(" ");
                student.add(new Student(Integer.parseInt(record[0]), record[1], record[2]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Student element : student) {
            System.out.println(element);
        }

    }

}
