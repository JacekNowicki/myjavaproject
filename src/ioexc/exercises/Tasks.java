package ioexc.exercises;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Tasks {
    public static void main(String[] args) {
        //#1
       // createTestFolders();

        //#2
       // listFoldersAndFiles();

        //#3
      //  deleteFiles();

        //#4
       // writeAndReadWithFiles();

        //#5
       // writeAndReadObjectsWithFiles();
    }

    /**
     * 1. Stwórz katalog testowy do ćwiczeń z systemem plików.
     * Wybierz katalog główny i stwórz w nim 3 foldery (A, B, C) i pliki (I.txt, II.txt)
     * W folderze A dodaj dwa podfoldery: A1 i A2.
     * Przenieś plik I.txt do folderu B
     * Skopiuj plik II.txt do folderów: A, A2, B i C
     */
    private static void createTestFolders() {
        Path testFolder = Paths.get("c:\\testtest");
        System.out.println("testfolder = " + testFolder.toAbsolutePath());

        Path folderA = testFolder.resolve("folderA");
        Path folderA1 = folderA.resolve("folderA1");
        Path folderA2 = folderA.resolve("folderA2");
        Path folderB = testFolder.resolve("folderB");
        Path folderC = testFolder.resolve("folderC");
        Path fileI = testFolder.resolve("I.txt");
        Path fileII = testFolder.resolve("II.txt");

        try {
            Files.createDirectories(folderA);
            Files.createDirectories(folderA1);
            Files.createDirectories(folderA2);
            Files.createDirectories(folderB);
            Files.createDirectories(folderC);

           if (Files.notExists(fileI)) {
               Files.createFile(fileI);
           }

           if ((Files.notExists(fileII))) {
               Files.createFile(fileII);
           }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 2. Wypisz na konsolę ścieżki do wszystkich folderów i podfolderów z katalogu testowego
     * Następnie wypisz wszystkie pliki znajdujące się w folderze B
     */
    private static void listFoldersAndFiles() {

    }

    /**
     * 3. Usuń katalog C z katalogu bazowego
     */
    private static void deleteFiles() {

    }

    /**
     * 4. Napisz kilka linijek tekstu do pliku A/II.txt
     * Następnie odczytaj tekst i wyświetl w konsoli.
     */
    private static void writeAndReadWithFiles() {
        Path filePath = Paths.get("C:/testtest/abcd.txt");
        try {
            BufferedWriter writer = Files.newBufferedWriter(filePath);{
                writer.write("Hello");
                writer.write("World");
                writer.close(); //file must be closed to get effect
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 5. Utwórz obiekt klasy Student.
     * Zapisz wartości pól ze stworzonego obiektu do pliku: {nazwa_studenta}.txt znajdującego się w folderze A/A1
     * Następnie odczytaj wartości z pliku i na ich podstawie stwórz nowy obiekt klasy Student.
     */
    private static void writeAndReadObjectsWithFiles() {

    }
}
