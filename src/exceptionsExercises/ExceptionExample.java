package exceptionsExercises;

import java.util.Scanner;

public class ExceptionExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print(">>> ");
            String input = scanner.nextLine();

            if ("q".equalsIgnoreCase(input)) {
                break;
            }

            try {
                int num = getNumber(input);
                double sqrt = getSqrt(num);
                System.out.println("Sqrt for " + num + " = " + sqrt);
            } catch (MyException e) {
                 System.out.println("Number can't be less than 0!");
            } catch (NumberFormatException e) {
                System.err.println("NumberFormatException occurred! Default value (0) will be used.");

            } finally {
                System.out.println("We will continue in loop ;)");
            }
        }
        ;
    }

    private static int getNumber(String input) throws NumberFormatException {
        return Integer.parseInt(input);
    }

    private static double getSqrt(int num) throws MyException {
        if (0 > num) {
            throw new MyException();
        }
        return Math.sqrt(num);
    }
}
