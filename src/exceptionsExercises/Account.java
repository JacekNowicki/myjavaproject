package exceptionsExercises;

import java.util.Scanner;

public class Account {

    public static void main(String[] args) {

        double balance = 0;
        double takeAway = 0;

        Scanner entry = new Scanner(System.in);
        System.out.println("How much there is on your account?");
        balance=entry.nextDouble();

        System.out.println("How much do you want to withdraw?");
        takeAway = entry.nextDouble();

        Account account = new Account();
        System.out.println("Zostalo "+account.withdraw(balance,takeAway));
    }

    public double withdraw (double balance, double takeAway){
        double operation = balance - takeAway;

        if (operation<0) throw new InsufficientFunds("No available funds!");

        return operation;
    }

}
