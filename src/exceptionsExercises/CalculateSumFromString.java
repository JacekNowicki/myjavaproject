package exceptionsExercises;

import java.util.Scanner;

public class CalculateSumFromString {

    public static void main(String[] args) {

        Scanner entry = new Scanner(System.in);
        String entryText = ((entry.nextLine()).trim());

        int arraySize = (entryText.split(" ").length);
        String word = "";
        int counter = 0;

        int[] table = new int[arraySize];

        for (int i = 0; i < (entryText.length()); i++) {


            if (entryText.charAt(i) == ' ') {

                try {
                    table[counter++] = Integer.parseInt(word);
                } catch (NumberFormatException e) {
                    System.out.println("element nr" + counter + " nie jest liczba, przypisano zero");
                    //word="0";
                    table[counter] = 0;
                }
                word = "";
                i++;
            }
            word = word + entryText.charAt(i);

            try {
                if ((entryText.length() - 1) == i) {
                    table[counter] = Integer.parseInt(word);
                }
            } catch (NumberFormatException e) {
                System.out.println("ostatni element nie jest liczba, przypisano zero");
                //word="0";
                table[counter] = 0;
            }
        }

        for (int element : table) {
            System.out.print(element + ", ");
        }

        try {System.out.println(" - suma to: " + SummingUp(table));}
        catch (ExceptionSumNegative ESN){
            System.out.println("Sum is negative!");

        }
    }

    public static int SummingUp(int[] items) throws ExceptionSumNegative{
        int summary = 0;
        for (int elem : items) {
            summary = summary + elem;
        }
        if (summary<0){throw new ExceptionSumNegative("Sum is below zero" );}
        return summary;

    }
}
