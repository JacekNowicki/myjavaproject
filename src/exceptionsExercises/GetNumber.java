package exceptionsExercises;

import java.util.InputMismatchException;
import java.util.Scanner;

public class GetNumber extends InputMismatchException{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = 0;
        try {
            number = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("to nie jest numer");
            number = 100;
        }
        finally {
            System.out.println("wszystko poszlo ok");
        }
        System.out.println(number);
    }
}
