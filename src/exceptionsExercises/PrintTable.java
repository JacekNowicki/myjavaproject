package exceptionsExercises;

import java.util.InputMismatchException;
import java.util.Scanner;

public class PrintTable {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] fruits = new String[]{"apple", "organe", "mango", "cheery"};
        int index;

        try {
            index = scanner.nextInt();
            System.out.println(fruits[index]);

        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("liczba musi byc 0-3");

        } catch (InputMismatchException e) {

            System.out.println("to nie jest numer");
        }

        finally {
            System.out.println("applikacja nie wysypala sie");
        }


    }
}

